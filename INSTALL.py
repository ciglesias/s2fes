# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 17:20:14 2014

@author: cristovao
"""

import os 

# Open a file
fo = open(str(os.getcwd())+"/bighousetools.desktop", "w")


inform="""
Installing the software BigHouse's tools.

To use this software, you need of:
    
astroml 
    https://pypi.python.org/pypi/astroML/
    http://www.astroml.org/

scikit-learn
    https://pypi.python.org/pypi/scikit-learn/0.14.1


Always execute the INSTALL.py in the same folder that big_house_tools.py and bht_icon.png

"""








fo.write(
"""
[Desktop Entry]
Name=sorteio
Comment=Big House's Tools
"""
+
"Exec="+str(os.getcwd())+"/big_house_tools.py"
+"\n"+
"Icon="+str(os.getcwd())+"/bht_icon.png"
+
"""
#Terminal=true
Type=Application
Name[en_US]=Big House's Tools
Name[pt_BR]=Big House's Tools
"""
)


os.system(" chmod +x "+str(os.getcwd())+"/bighousetools.desktop")
os.system(" ln -s "+str(os.getcwd())+"/bighousetools.desktop  "  + 
str(os.sep.join((os.path.expanduser("~"), 'Desktop')))+
"/bighousetools.desktop"  )



# Close opend file
fo.close()

print ":)   Installation concluded  (;"







