#!/usr/bin/python
# -*- coding: utf-8 -*-

from numpy import genfromtxt
import sys,string, os
import numpy as np
import math
from numpy.random import uniform, seed
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pylab import *
from matplotlib import *
from numpy.random import randn
from matplotlib.patches import Patch
from optparse import OptionParser
import wx
from scipy import stats
from astroML.plotting import hist
from optHist2d_OO import Opthist2d


#from compute_hist_bin_width import compute_hist_bins



class Big_house_tools(wx.Frame):
    """A set of tools to analyse molecular dynamics results."""
           
    def __init__(self, *args, **kw):
        super(Big_house_tools, self).__init__(*args, **kw) 
        
        self.InitUI()
        
        #path to the files of the rmsd and rg
        self.path_rmsd=""
        self.path_rg=""
        self.path_op1=""
        self.path_op2=""
        self.path_op1op2=""        
        self.array_bins_edges=[]   
        self.str_selec="" 
        self.str_selec1="" 
        self.type_file_selec_op1=""
        self.type_file_selec_op2=""
        self.type_file_selec_op1op2=""        
        self.aut_manu_selec_fes=""
        self.aut_manu_selec_btw=""        
        self.cell_populated=[]
        self.data_op1=[]            #save the data of order parameter 1
        self.data_op2=[]            #save the data of order parameter 2
        self.data_time=[]        
        self.name_data_op1=""
        self.name_data_op2=""
        self.str_output_hist_inf=""
        self.export_fes_file=False
        self.plot_fes = False    
        self.fes_computed = False 
        self.setting=[] #(minv1, maxv1, minv2, maxv2, H,DG,xedges,yedges ,extent)  
        self.output_path_fesfile=""
        self.aut_manu_selec_bins=""
        self.str_bestbin =""         
        #self.button1;
        

        
    def InitUI(self):   

        #pnl = wx.Panel(self)

        self.notebook_1 = wx.Notebook(self, -1, style=0)
        self.notebook_1_pane_1 = wx.Panel(self.notebook_1, -1)
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, -1)
        self.notebook_1_pane_3 = wx.Panel(self.notebook_1, -1)
        self.notebook_1_pane_4 = wx.Panel(self.notebook_1, -1)

        #self.button_1 = wx.Button(self.notebook_1_pane_1, -1, "button_1")

        self.__set_properties()
        self.__do_layout()
        
        #---------------------------------- labels ------------------------------------   

        #Pane1
        #self.filenamerg = wx.StaticText(self.notebook_1_pane_1, label='RG file (input): ', pos=(250, 115))
        #self.filenamermsd = wx.StaticText(self.notebook_1_pane_1, label='RMSD file (input): ', pos=(250, 145))
        wx.StaticText(self.notebook_1_pane_1, label='Input Files', pos=(20,20))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Files', pos=(400, 85))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Type file', pos=(200, 85))
        #self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Only Two column', pos=(180, 180))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Order Parameter name (OP1)', pos=(675, 110))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Order Parameter name (OP2)', pos=(675, 140))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='OP1 name             OP2 name', pos=(570, 200))


        #Pane2
        wx.StaticText(self.notebook_1_pane_2, label='Max and Min Values', pos=(450,20))
        self.distr_info=wx.StaticText(self.notebook_1_pane_2, label='Information', pos=(20,690))
        #wx.StaticText(self.notebook_1_pane_2, label='Output file name: ', pos=(550, 20))
        self.max1p2=wx.StaticText(self.notebook_1_pane_2, label='OP1 max: ', pos=(450, 90))
        self.min1p2=wx.StaticText(self.notebook_1_pane_2, label='OP1 min: ', pos=(450, 120))
        self.max2p2=wx.StaticText(self.notebook_1_pane_2, label='OP2 max: ', pos=(450, 150))
        self.min2p2=wx.StaticText(self.notebook_1_pane_2, label='OP2 min: ', pos=(450, 180))

        #Pane3
        wx.StaticText(self.notebook_1_pane_3, label='Max and Min Values', pos=(300,20))
        wx.StaticText(self.notebook_1_pane_3, label='OP1 and OP2 Bins', pos=(20,20))


        self.label_nbinsop1=wx.StaticText(self.notebook_1_pane_3, label='Number of bins to OP1\n\nmin:\t\t\tmax:', pos=(20,80))               
        self.label_nbinsop2=wx.StaticText(self.notebook_1_pane_3, label='Number of bins to OP2\n\nmin:\t\t\tmax:', pos=(20,145))                
        self.label_searchbins=wx.StaticText(self.notebook_1_pane_3, label='Searching for bins \nbetween 1 and 100!', pos=(20,80))        
        self.label_op1bin=wx.StaticText(self.notebook_1_pane_3, label='OP1 bin : ', pos=(20, 80))
        self.label_op2bin=wx.StaticText(self.notebook_1_pane_3, label='OP2 bin: ', pos=(20, 105))
        wx.StaticText(self.notebook_1_pane_3, label='Temperature: ', pos=(60, 210))
        
        self.label_cfilename=wx.StaticText(self.notebook_1_pane_3, label='Output file name', pos=(670, 55))        
        self.label_cfolder=wx.StaticText(self.notebook_1_pane_3, label='Choose one folder', pos=(670, 85))        
        self.pt_fes=wx.StaticText(self.notebook_1_pane_3, label='Plot title: ', pos=(550, 180)) #input Plot title 
        self.max1p3= wx.StaticText(self.notebook_1_pane_3, label='OP1 max: ', pos=(300, 90))
        self.min1p3= wx.StaticText(self.notebook_1_pane_3, label='OP1 min: ', pos=(300, 120))
        self.max2p3= wx.StaticText(self.notebook_1_pane_3, label='OP2 max: ', pos=(300, 150))
        self.min2p3= wx.StaticText(self.notebook_1_pane_3, label='OP2 min: ', pos=(300, 180))

        #Pane4

        wx.StaticText(self.notebook_1_pane_4, label='Coordinates', pos=(300,20))
        wx.StaticText(self.notebook_1_pane_4, label='Point', pos=(30,20))
        wx.StaticText(self.notebook_1_pane_4, label='Nº output points: ', pos=(20, 105))
        #wx.StaticText(self.notebook_1_pane_4, label='Output file name: ', pos=(550, 20))
        #wx.StaticText(self.notebook_1_pane_4, label='Plot title: ', pos=(550, 180))
        self.x1 = wx.StaticText(self.notebook_1_pane_4, label='x1: ', pos=(300, 90)) #input cell coordinates
        self.x2 = wx.StaticText(self.notebook_1_pane_4, label='x2: ', pos=(300, 120))
        self.y1 = wx.StaticText(self.notebook_1_pane_4, label='y1: ', pos=(300, 150))
        self.y2 = wx.StaticText(self.notebook_1_pane_4, label='y2: ', pos=(300, 180))
        


        #---------------------------------- output ------------------------------------
        self.outputbox1 = wx.TextCtrl(self.notebook_1_pane_2, style=wx.TE_MULTILINE, size=(400,590), pos=(30, 90))
        self.outputbox2 = wx.TextCtrl(self.notebook_1_pane_3, style=wx.TE_MULTILINE, size=(450,200), pos=(60, 300))
        self.outputbox3 = wx.TextCtrl(self.notebook_1_pane_4, style=wx.TE_MULTILINE, size=(550,390), pos=(30, 260))


        #---------------------------------- input ------------------------------------

        distros = ['Manual max and min', 'Automatic max and min']
        options1 = ['Cell most populated', 'Region']
        options2 = ['mean','mode','median']
        options3 = ['gromacs rmsd', 'gromacs rg', 'gromacs PCA - proj1D', 'only one column' ]
        options4 = ['gromacs PCA - proj2D', 'only two column' ]
        options5 = [ 'Estimating Automatically Bins (Default)','Estimating Automatically Bins (Setting)','Manual Bins' ]        

        cb  = wx.ComboBox(self.notebook_1_pane_2,  pos=(450, 40),  choices=distros,  style=wx.CB_READONLY)
        cb2 = wx.ComboBox(self.notebook_1_pane_3,  pos=(300, 40),  choices=distros,  style=wx.CB_READONLY)
        cb3 = wx.ComboBox(self.notebook_1_pane_4,  pos=(300, 40),  choices=options1, style=wx.CB_READONLY)
        cb4 = wx.ComboBox(self.notebook_1_pane_4,  pos=(30, 40),   choices=options2, style=wx.CB_READONLY)
        cb5 = wx.ComboBox(self.notebook_1_pane_1,  pos=(120, 110), choices=options3, style=wx.CB_READONLY)
        cb6 = wx.ComboBox(self.notebook_1_pane_1,  pos=(120, 140), choices=options3, style=wx.CB_READONLY)
        cb7 = wx.ComboBox(self.notebook_1_pane_1,  pos=(120, 170), choices=options4, style=wx.CB_READONLY)
        cb8 = wx.ComboBox(self.notebook_1_pane_3,  pos=(20, 40),  choices=options5,  style=wx.CB_READONLY)


        self.sc = wx.SpinCtrl(self.notebook_1_pane_3, value='0', pos=(150, 75), size=(60, -1)) #bin rg 
        self.sc.SetRange(-459, 1000)
        self.sc2 = wx.SpinCtrl(self.notebook_1_pane_3, value='0', pos=(150, 100), size=(60, -1)) #bin rmsd
        self.sc2.SetRange(-459, 1000)
        self.sc3 = wx.SpinCtrl(self.notebook_1_pane_3, value='0', pos=(160, 210), size=(60, -1)) #temperatura
        self.sc3.SetRange(-459, 1000)

        self.sc4 = wx.SpinCtrl(self.notebook_1_pane_4, value='0', pos=(150, 100), size=(60, -1)) #'Nº output points: '
        self.sc4.SetRange(1, 20000)

        self.output_filename_fes = wx.TextCtrl(self.notebook_1_pane_3, pos=(550, 50), size=(120, -1))  #output name
        #self.output_bw = wx.TextCtrl(self.notebook_1_pane_2, pos=(550, 40), size=(120, -1))  #output name
        #self.output_bw = wx.TextCtrl(self.notebook_1_pane_4, pos=(550, 40), size=(120, -1))  #output name

        self.op1_name_onecol = wx.TextCtrl(self.notebook_1_pane_1, pos=(530, 110), size=(120, -1))  # op1 name
        self.op2_name_onecol = wx.TextCtrl(self.notebook_1_pane_1, pos=(530, 140), size=(120, -1))  # op2 name
        self.op1_name_twocol = wx.TextCtrl(self.notebook_1_pane_1, pos=(530, 170), size=(120, -1))  # op1 name
        self.op2_name_twocol = wx.TextCtrl(self.notebook_1_pane_1, pos=(650, 170), size=(120, -1))  # op2 name

        self.output_title_fes = wx.TextCtrl(self.notebook_1_pane_3, pos=(550, 200), size=(120, -1))  #plot title name
        #self.output_title = wx.TextCtrl(self.notebook_1_pane_4, pos=(550, 200), size=(120, -1))  #plot title name

        self.input_max1p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 90), size=(60, -1))  #input max
        self.input_min1p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 120), size=(60, -1))  #input min
        self.input_max2p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 150), size=(60, -1))  #input max
        self.input_min2p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 180), size=(60, -1))  #input min

        self.input_maxbinOP1 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(160, 110), size=(40, -1))  #input max
        self.input_minbinOP1 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(70, 110), size=(40, -1))  #input min
        self.input_maxbinOP2 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(160, 170), size=(40, -1))  #input max
        self.input_minbinOP2 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(70, 170), size=(40, -1))  #input min


        self.input_max1p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 90), size=(60, -1))  #input max
        self.input_min1p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 120), size=(60, -1))  #input min
        self.input_max2p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 150), size=(60, -1))  #input max
        self.input_min2p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 180), size=(60, -1))  #input min

        self.input_x1 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 90),  size=(120, -1))  #input max
        self.input_x2 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 120), size=(120, -1))  #input min
        self.input_y1 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 150), size=(120, -1))  #input max
        self.input_y2 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 180), size=(120, -1))  #input min

        #---------------------------------- buttons ------------------------------------

        self.btn_op1 = wx.Button(self.notebook_1_pane_1, label="Choose a file", pos=(340, 110), size=(150, -1)) #input file rg
        self.btn_op2 = wx.Button(self.notebook_1_pane_1, label="Choose a file", pos=(340, 140), size=(150, -1)) #input file rmsd
        self.btn_op1_op2 = wx.Button(self.notebook_1_pane_1, label="Choose a file", pos=(340, 170), size=(150, -1)) #input file rmsd

        btn_bw = wx.Button(self.notebook_1_pane_2, label='Estimating bin width', pos=(30, 40))
        #btn_bw = wx.Button(self.notebook_1_pane_3, label='Estimating bins', pos=(30, 40))
        #self.button1 = wx.Button(self.notebook_1_pane_2, label='Manual max and min', pos=(250, 40),size=(200, -1))

        btn_fes = wx.Button(self.notebook_1_pane_3, label='Compute FES', pos=(30, 260))
        btn_points = wx.Button(self.notebook_1_pane_4, label='Compute points', pos=(30, 150))
        #btn.SetFocus()
        

        #dirDlgBtn = wx.Button(self.notebook_1_pane_2, label="output folder", pos=(550, 80))
        self.dirDlgBtn1 = wx.Button(self.notebook_1_pane_3, label="output folder", pos=(550, 80))
        #dirDlgBtn2 = wx.Button(self.notebook_1_pane_4, label="output folder", pos=(550, 80))
        btn_setexportfes = wx.Button(self.notebook_1_pane_3, label="Setting to export FES file", pos=(550, 20))
        btn_settoplot = wx.Button(self.notebook_1_pane_3, label="Setting to plot", pos=(550, 150))
        self.btn_exportfes = wx.Button(self.notebook_1_pane_3, label="Export", pos=(550, 115))
        self.btn_plotfes = wx.Button(self.notebook_1_pane_3, label="Plot", pos=(550, 230))
        
        #---------------------------------- Hide -----------------------------------------        
        self.dirDlgBtn1.Hide()
        self.btn_exportfes.Hide()
        self.output_filename_fes.Hide()  
        self.label_cfilename.Hide()
        self.label_cfolder.Hide()
        self.output_filename_fes.Hide()        
        self.pt_fes.Hide()        
        self.output_title_fes.Hide()
        self.btn_plotfes.Hide()      
        
        #----
        self.sc.Hide()
        self.sc2.Hide()
        self.label_nbinsop1.Hide()
        self.label_nbinsop2.Hide()
        self.label_searchbins.Hide()
        self.label_op1bin.Hide()
        self.label_op2bin.Hide()
        self.input_maxbinOP1.Hide()
        self.input_minbinOP1.Hide()
        self.input_maxbinOP2.Hide()
        self.input_minbinOP2.Hide()
        
        
        
        
        #---------------------------------- events ------------------------------------

        btn_setexportfes.Bind(wx.EVT_BUTTON, self.button_setting_export_fes)
        btn_settoplot.Bind(wx.EVT_BUTTON, self.button_setting_to_plot)    
        btn_fes.Bind(wx.EVT_BUTTON, self.OnCompute_fes)
        btn_bw.Bind(wx.EVT_BUTTON, self.OnCompute_bw)
        btn_points.Bind(wx.EVT_BUTTON, self.OnCompute_btn_points)
        self.btn_exportfes.Bind(wx.EVT_BUTTON, self.button_export_fes)        
        self.btn_plotfes.Bind(wx.EVT_BUTTON, self.button_plotfes)
        
       # self.button1.Bind(wx.EVT_BUTTON, self.compute_mm_or_give_mm)
        
        self.btn_op1.Bind(wx.EVT_BUTTON, self.onOpenFile_op1) #self.onOpenFile_op1
        self.btn_op2.Bind(wx.EVT_BUTTON, self.onOpenFile_op2)
        self.btn_op1_op2.Bind(wx.EVT_BUTTON, self.onOpenFile_two_column)

        cb.Bind(wx.EVT_COMBOBOX, self.OnSelect)
        cb2.Bind(wx.EVT_COMBOBOX, self.OnSelect2) #FES
        cb3.Bind(wx.EVT_COMBOBOX, self.OnSelect3)
        cb4.Bind(wx.EVT_COMBOBOX, self.OnSelect4)
        cb5.Bind(wx.EVT_COMBOBOX, self.OnSelect_cd5)
        cb6.Bind(wx.EVT_COMBOBOX, self.OnSelect_cd6)
        cb7.Bind(wx.EVT_COMBOBOX, self.OnSelect_cd7)  
        cb8.Bind(wx.EVT_COMBOBOX, self.OnSelect_cd8)          
        

        #dirDlgBtn.Bind(wx.EVT_BUTTON, self.onDir)
        self.dirDlgBtn1.Bind(wx.EVT_BUTTON, self.Dirtoexport)

        #---------------------------------- configuration of GUI ------------------------------------
                      #x ,  y 
        self.SetSize((900, 600))
        self.SetTitle("Big House's Tools")
        self.Centre()
        self.Show(True)  




 

    def onDir(self, event):
        """
        Show the DirDialog and print the user's choice to stdout
        """
        dlg = wx.DirDialog(self, "Choose a directory:",
                           style=wx.DD_DEFAULT_STYLE
                           #| wx.DD_DIR_MUST_EXIST
                           #| wx.DD_CHANGE_DIR
                           )
        if dlg.ShowModal() == wx.ID_OK:
            print "You chose %s" % dlg.GetPath()
        dlg.Destroy()

    def Dirtoexport(self, event):
        """
        Show the DirDialog and print the user's choice to stdout
        """
        dlg = wx.DirDialog(self, "Choose a directory:",
                           style=wx.DD_DEFAULT_STYLE
                           #| wx.DD_DIR_MUST_EXIST
                           #| wx.DD_CHANGE_DIR
                           )
        if dlg.ShowModal() == wx.ID_OK:
            print "You chose %s" % dlg.GetPath()
            self.label_cfolder.SetLabel("%s" % dlg.GetPath())
            self.output_path_fesfile=dlg.GetPath()
        dlg.Destroy()
        

    def OnSelect(self, e):
        
        self.aut_manu_selec_btw = e.GetString()
        #self.st.SetLabel(i)

        if self.aut_manu_selec_btw =='Automatic max and min':
                #self.button1.SetLabel('Automatic max and min')
                self.max1p2.SetLabel("")
                self.min1p2.SetLabel("")
                self.max2p2.SetLabel("")
                self.min2p2.SetLabel("")
                self.input_max1p2.SetValue("")
                self.input_min1p2.SetValue("")
                self.input_max2p2.SetValue("")
                self.input_min2p2.SetValue("")
                self.input_max1p2.Hide()
                self.input_min1p2.Hide()
                self.input_max2p2.Hide()
                self.input_min2p2.Hide()
                
        else:
                #self.button1.SetLabel('Manual max and min')
                self.max1p2.SetLabel('OP1 max: ')
                self.min1p2.SetLabel('OP1 min: ')
                self.max2p2.SetLabel('OP2 max: ')
                self.min2p2.SetLabel('OP2 min: ')
                self.input_max1p2.Show()
                self.input_min1p2.Show()
                self.input_max2p2.Show()
                self.input_min2p2.Show()



    def OnSelect2(self, e):
        
        self.aut_manu_selec_fes = e.GetString()
        #self.st.SetLabel(i)

        if self.aut_manu_selec_fes =='Automatic max and min':
                #self.button1.SetLabel('Automatic max and min')
                self.max1p3.SetLabel("")
                self.min1p3.SetLabel("")
                self.max2p3.SetLabel("")
                self.min2p3.SetLabel("")
                self.input_max1p3.SetValue("")
                self.input_min1p3.SetValue("")
                self.input_max2p3.SetValue("")
                self.input_min2p3.SetValue("")
                self.input_max1p3.Hide()
                self.input_min1p3.Hide()
                self.input_max2p3.Hide()
                self.input_min2p3.Hide()


                
        else:
                #self.button1.SetLabel('Manual max and min')

                self.max1p3.SetLabel('OP1 max: ')
                self.min1p3.SetLabel('OP1 min: ')
                self.max2p3.SetLabel('OP2 max: ')
                self.min2p3.SetLabel('OP2 min: ')
                self.input_max1p3.Show()
                self.input_min1p3.Show()
                self.input_max2p3.Show()
                self.input_min2p3.Show()

    def OnSelect3(self, e):
        
        self.str_selec = e.GetString()
        #self.st.SetLabel(i)

        if self.str_selec =='Cell most populated':
                #self.button1.SetLabel('Automatic max and min')
                self.x1.SetLabel("")
                self.x2.SetLabel("")
                self.y1.SetLabel("")
                self.y2.SetLabel("")

                self.input_x1.SetValue("")
                self.input_x2.SetValue("")
                self.input_y1.SetValue("")
                self.input_y2.SetValue("")

                self.input_x1.Hide()
                self.input_x2.Hide()
                self.input_y1.Hide()
                self.input_y2.Hide()
                
        else:
                #self.button1.SetLabel('Manual max and min')

                self.x1.SetLabel('x1:')
                self.x2.SetLabel('x2:')
                self.y1.SetLabel('y1:')
                self.y2.SetLabel('y2:')
                self.input_x1.Show()
                self.input_x2.Show()
                self.input_y1.Show()
                self.input_y2.Show()


    def OnSelect4(self, e):
        
        self.str_selec1 = e.GetString()
        #self.st.SetLabel(i)

#        if self.str_selec =='mode':
#            self.str_selec1='mode'

#        if self.str_selec =='median':
#            self.str_selec1='median'

#        if self.str_selec =='mean':
#            self.str_selec1 ='median'  


    def OnSelect_cd5(self, e):
        "Get the type file that was selected in cd5"        
        self.type_file_selec_op1 = e.GetString()

    def OnSelect_cd6(self, e):
        "Get the type file that was selected in cd6"
        self.type_file_selec_op2 = e.GetString()

    def OnSelect_cd7(self, e):
        "Get the type file that was selected in cd6"
        self.type_file_selec_op1op2 = e.GetString()



    def OnSelect_cd8(self, e):
        
        self.aut_manu_selec_bins = e.GetString()
        # 'Estimating Automatically Bins (Default)','Estimating Automatically Bins (Setting)','Manual Bins' 

        if self.aut_manu_selec_bins =='Estimating Automatically Bins (Default)':
             
             self.sc.Hide()
             self.sc2.Hide()
             self.label_nbinsop1.Hide()
             self.label_nbinsop2.Hide()
             self.label_searchbins.Show()
             self.label_op1bin.Hide()
             self.label_op2bin.Hide()
             self.input_maxbinOP1.Hide()
             self.input_minbinOP1.Hide()
             self.input_maxbinOP2.Hide()
             self.input_minbinOP2.Hide()

                
        elif self.aut_manu_selec_bins =='Estimating Automatically Bins (Setting)':
                
             self.sc.Hide()
             self.sc2.Hide()
             self.label_nbinsop1.Show()
             self.label_nbinsop2.Show()
             self.label_searchbins.Hide()
             self.label_op1bin.Hide()
             self.label_op2bin.Hide()
             self.input_maxbinOP1.Show()
             self.input_minbinOP1.Show()
             self.input_maxbinOP2.Show()
             self.input_minbinOP2.Show()
                
        else:
             self.sc.Show()
             self.sc2.Show()
             self.label_nbinsop1.Hide()
             self.label_nbinsop2.Hide()
             self.label_searchbins.Hide()
             self.label_op1bin.Show()
             self.label_op2bin.Show()
             self.input_maxbinOP1.Hide()
             self.input_minbinOP1.Hide()
             self.input_maxbinOP2.Hide()
             self.input_minbinOP2.Hide()
                
             

    def compute_mm_or_give_mm(self,e):
            if self.button1.GetLabel() =='Manual max and min':
                self.button1.SetLabel('Automatic max and min')
                self.max1p2.SetLabel("")
                self.min1p2.SetLabel("")
                self.max2p2.SetLabel("")
                self.min2p2.SetLabel("")
                self.input_max1p2.SetValue("")
                self.input_min1p2.SetValue("")
                self.input_max2p2.SetValue("")
                self.input_min2p2.SetValue("")

                self.input_max1p2.Hide()
                self.input_min1p2.Hide()
                self.input_max2p2.Hide()
                self.input_min2p2.Hide()


                
            else:
                self.button1.SetLabel('Manual max and min')

                self.max1p2.SetLabel('OP1 max: ')
                self.min1p2.SetLabel('OP1 min: ')
                self.max2p2.SetLabel('OP2 max: ')
                self.min2p2.SetLabel('OP2 min: ')
                self.input_max1p2.Show()
                self.input_min1p2.Show()
                self.input_max2p2.Show()
                self.input_min2p2.Show()


    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetTitle(("frame_1"))
        self.SetBackgroundColour(wx.Colour(255, 248, 84))
        self.SetForegroundColour(wx.Colour(72, 69, 255))
        self.SetFocus()
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        self.notebook_1.AddPage(self.notebook_1_pane_1, ("Input and Output"))
        self.notebook_1.AddPage(self.notebook_1_pane_2, ("Estimating bin width"))
        self.notebook_1.AddPage(self.notebook_1_pane_3, ("Compute FES"))
        self.notebook_1.AddPage(self.notebook_1_pane_4, ("Getting Structure"))
        sizer_1.Add(self.notebook_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        sizer_1.SetSizeHints(self)
        self.Layout()
        # end wxGlade



    def onOpenFile_op1(self, event):     
        """
        Create and show the Open FileDialog #GetDirectory
        """
        tfs= self.type_file_selec_op1
        nm = self.op1_name_onecol.GetValue()
        
        self.path_op1=""
        self.data_op1=[]        
        
        if tfs == "":
            self.ShowMessage1()
            
        elif  nm == "":
            self.ShowMessage2() 
            
        elif tfs== 'gromacs rmsd' or tfs == 'gromacs rg' or  tfs =='gromacs PCA - proj1D' or tfs =='only one column':
        
            dlg = wx.FileDialog(
                self, message="Choose a "+tfs+" file (op1)",           
                defaultFile="",
                wildcard="Python source (*.xvg; *.xvg)|*.xvg;*.xvg| All files (*.*)|*.*" ,                                        
                style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
                )
            if dlg.ShowModal() == wx.ID_OK:
                paths = dlg.GetPath()
                self.path_op1=paths                         
                print "You chose the following file(s):\n" + paths
                
                if nm != "":
                    self.name_data_op1 = nm
                    print "You chose the OP1 name: " + nm            
            
                if tfs== 'gromacs rmsd':
                    self.data_op1=self.get_rmsd(paths)['rmsd']
                    self.data_time=self.get_rmsd(paths)['time']                    
                elif tfs == 'gromacs rg':
                    self.data_op1=self.get_rg(paths)['rg']
                    self.data_time=self.get_rg(paths)['time']  
                elif  tfs =='gromacs PCA - proj1D':
                    try:                        
                        self.data_op1=self.get_pca(paths,self.skip_header(paths))['pca']
                        self.data_time=self.get_pca(paths,self.skip_header(paths))['time']
                    except ValueError:
                        self.ShowMessage9()
                elif  tfs =='only one column':
                    self.data_op1=self.get_data_one_col(paths,"op1")
                    self.data_time=[]  
                    print self.data_op1

                self.btn_op1.SetLabel(paths.split('/')[-1])
                
                print "You chose the following type file:" + tfs   
                
            dlg.Destroy()


    def onOpenFile_op2(self, event):      #replace to order parameter 1
        """
        Create and show the Open FileDialog #GetDirectory
        """
        tfs= self.type_file_selec_op2
        nm = self.op2_name_onecol.GetValue()
        
        self.path_op2=""
        self.data_op2=[]  
        
        if tfs == "":
            self.ShowMessage1()
            
        elif  nm == "":
            self.ShowMessage3() 
            
        elif tfs== 'gromacs rmsd' or tfs == 'gromacs rg' or  tfs =='gromacs PCA - proj1D' or tfs =='only one column':
        
            dlg = wx.FileDialog(
                self, message="Choose a "+tfs+" file (op2)",           
                defaultFile="",
                wildcard="Python source (*.xvg; *.xvg)|*.xvg;*.xvg| All files (*.*)|*.*" ,                                        
                style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
                )
            if dlg.ShowModal() == wx.ID_OK:
                paths = dlg.GetPath()
                self.path_op2=paths                         
                print "You chose the following file(s):\n" + paths
                
                if nm != "":
                    self.name_data_op2 = nm
                    print "You chose the OP2 name: " + nm            
            
                if tfs== 'gromacs rmsd':
                    self.data_op2=self.get_rmsd(paths)['rmsd']
                    self.data_time=self.get_rmsd(paths)['time']
                    
                elif tfs == 'gromacs rg':
                    self.data_op2=self.get_rg(paths)['rg']
                    self.data_time=self.get_rg(paths)['time']                    
                elif  tfs =='gromacs PCA - proj1D':
                    try:                        
                        self.data_op2=self.get_pca(paths,self.skip_header(paths))['pca']
                        self.data_time=self.get_pca(paths,self.skip_header(paths))['time']
                    except ValueError:
                        self.ShowMessage9()                
                elif  tfs =='only one column':
                    self.data_op2=self.get_data_one_col(paths,"op2")
                    self.data_time=[]
                    
                    
                self.btn_op2.SetLabel(paths.split('/')[-1])
                    
                print "You chose the following type file:" + tfs            

            dlg.Destroy()

    def onOpenFile_two_column(self, event):      
        """
        Create and show the Open FileDialog #GetDirectory
        """

        #'gromacs PCA - proj2D', 'only two column'         
        tfs= self.type_file_selec_op1op2
        self.data_op1=[]
        self.data_op2=[]
        
        if self.op1_name_twocol.GetValue()=="":
            self.ShowMessage4()             
            
        elif self.op2_name_twocol.GetValue() =="" : 
            self.ShowMessage5()   
          
        elif tfs  =="":
            self.ShowMessage1()
            
        elif    self.op1_name_twocol.GetValue()!="" and self.op2_name_twocol.GetValue()!="":
            dlg = wx.FileDialog(
                self, message="Choose a file with two column",           
                defaultFile="",
                wildcard="Python source (*.xvg; *.xvg)|*.xvg;*.xvg| All files (*.*)|*.*" ,                                        
                                        style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
                )
            if dlg.ShowModal() == wx.ID_OK:

                paths = dlg.GetPath()
                self.path_op1op2=paths                          
                print "You chose the following file(s):"+ paths

                self.name_data_op1 = self.op1_name_twocol.GetValue()
                self.name_data_op2 = self.op2_name_twocol.GetValue()
                print "You chose the OP1 name: " + self.name_data_op1
                print "You chose the OP2 name: " + self.name_data_op2                
    
                if tfs== 'only two column' :
                    
                    dataset_tmp=self.get_data_two_col(paths,"op1","op2")
                    self.data_op1=dataset_tmp["op1"]
                    self.data_op2=dataset_tmp["op2"]
                    self.data_time=[]
                    
                elif   tfs== 'gromacs PCA - proj2D':
                    
                    self.data_op1=self.get_pca_2d(paths,self.skip_header(paths))['pca1']
                    self.data_op2=self.get_pca_2d(paths,self.skip_header(paths))['pca2']
                    self.data_time=[]
                    
                self.btn_op1_op2.SetLabel(paths.split('/')[-1])


            dlg.Destroy()





    def button_setting_export_fes(self, e):
        """
        Hide and show the setting to export the FES file
        """
        
#        if (self.fes_computed == True):           
        
        if (self.export_fes_file==False): 

                self.dirDlgBtn1.Show()
                self.btn_exportfes.Show()                    
                self.output_filename_fes.Show()  
                self.label_cfilename.Show()
                self.label_cfolder.Show()
                self.export_fes_file=True
            
        elif (self.export_fes_file==True): 
            
                self.dirDlgBtn1.Hide()
                self.btn_exportfes.Hide()                    
                self.output_filename_fes.Hide()  
                self.label_cfilename.Hide()
                self.label_cfolder.Hide()
                self.export_fes_file=False
#        else:
            
#            self.ShowMessage7()



    def button_setting_to_plot(self, e):
        """
        Hide and show the setting to export the FES file
        """
        
       # if (self.fes_computed == True):
        
        if (self.plot_fes==False): 

                self.btn_plotfes.Show()  
                self.output_title_fes.Show()
                self.pt_fes.Show()            
                self.plot_fes=True
            
        elif (self.plot_fes==True):  
                
                self.btn_plotfes.Hide()                  
                self.output_title_fes.Hide()
                self.plot_fes=False
                self.pt_fes.Hide()

        #else:
            
         #   self.ShowMessage7()


#---------------------------- methods to show message ----------------------------------

    def ShowMessage1(self):
        wx.MessageBox('Select the type file!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage2(self):
        wx.MessageBox('Give a name to OP1!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage3(self):
        wx.MessageBox('Give a name to OP2!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage4(self):
        wx.MessageBox('Give a name to OP1!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage5(self):
        wx.MessageBox('Give a name to OP2!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage6(self):
        wx.MessageBox('Select manual or automatic max and min values!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage7(self):
        wx.MessageBox('Please, compute FES before!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage8(self):
        wx.MessageBox('Please, Select manual or automatic max and min values!', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage9(self):
        wx.MessageBox('Please, Check your input file. There may be some character in file end. Please remove it! ', 'Info', 
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage10(self):
        wx.MessageBox('The user should search the binwidth that is large than the sampling resolution of your data', 'Info', 
            wx.OK | wx.ICON_INFORMATION)




#---------------------------- methods to open input files ----------------------------------
# Create new methods to allow the user upload RMSD, RG, PC1, PC2, etc ...

    def skip_header(self,infilename):     #--> IMP - use this for all method beyond get_pca
        """return the number of the lines of a file header """
        
        f=open(infilename, 'r')
        n=0
        l=f.readline()
        while ("#" in l or "@" in l):
            n=n+1
            l=f.readline()
        f.close() 
        
        return n


    def get_pca(self,infilename,nlines):
        """Get the pca"""
        data_pca = genfromtxt(infilename,skip_header=nlines, 
                              dtype={'names':['time', 'pca'], 
                              'formats':[int,float]})
        return data_pca


    def get_pca_2d(self,infilename,nlines):
        """Get the pca"""
        data_pca = genfromtxt(infilename,skip_header=nlines, 
                              dtype={'names':['pca1', 'pca2'], 
                              'formats':[float,float]})
        return data_pca
                

    def get_rg(self,infilename):
        """Get the rg"""
        data_rg = genfromtxt(infilename,skip_header=22, 
                        dtype={'names':['time', 'rg','rgx','rgy','rgz'],
                       'formats':[int,float,float,float,float]})
        return data_rg

    def get_rmsd(self,infilename):    
        """Get the rmsd"""
        data_rmsd = genfromtxt(infilename,skip_header=13, 
                        dtype={'names':['time', 'rmsd'],
                       'formats':[int,float]})
        return data_rmsd

    def get_data_one_col(self,infilename,name):     
        """Get the data from file"""

        #data = genfromtxt(infilename, dtype={'names':[str(name)], 'formats':[float]})
        #problema aqui, -> [(1.82477,) (1.82132,) (1.82001,) ..., (1.65042,) (1.64056,) (1.64343,)]
        
        data = genfromtxt(infilename)
                                              
        return data

    def get_data_two_col(self,infilename,name1,name2):    
        """Get the data from file"""
        data = genfromtxt(infilename, 
                        dtype={'names':[str(name1),str(name2)],
                       'formats':[float,float]})
        return data

#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


        
    def button_export_fes(self, e):
        """Button to export fes    """
        #self.setting=(minv1, maxv1, minv2, maxv2, H,DG,xedges,yedges ,extent) 
        
        path=self.output_path_fesfile+"/"+self.output_filename_fes.GetValue()
        self.write_fes_file(path, self.setting[4], self.setting[5], self.setting[6], self.setting[7])
        
        
    def button_plotfes(self, e):
        """Button to plot fes    """
        # Use this to create a new plot
        #http://stackoverflow.com/questions/6508769/matplotlib-scatter-hist-with-stepfilled-histtype-in-histogram        
        
        path=self.output_path_fesfile+"/"+self.output_filename_fes.GetValue()  
        self.plot_fes_Contour(plt.cm.Spectral,path, self.setting[0],self.setting[1],self.setting[2],self.setting[3], 10)	
        self.plot_fes_hist2d(self.setting[8],self.setting[5])

    
        
    def OnCompute_fes(self, e):        
        """Execute the method to compute FES """
        
        
#--------------------------------------------------------- 'Estimating Automatically Bins (Default)','Estimating Automatically Bins (Setting)'

        if self.aut_manu_selec_bins=='Estimating Automatically Bins (Default)' or self.aut_manu_selec_bins=='Estimating Automatically Bins (Setting)' :
               
            x=self.data_op1            #save the data of order parameter 1
            y=self.data_op2
            H=Opthist2d(x,y)   
        
            dx=H.sample_resolution(H.data_x)
            dy=H.sample_resolution(H.data_y)

            
            if self.aut_manu_selec_bins=='Estimating Automatically Bins (Setting)' :  #----------------------------- IMP -> TERMINAR
                
                if ( dx > (H.x_max - H.x_min)/float(self.input_maxbinOP1.GetValue()) and dy > (H.y_max - H.y_min)/float(self.input_maxbinOP2.GetValue()) ):
                    
                    self.ShowMessage10()
                    
                Nx=H.arrayofbins(int(self.input_minbinOP1.GetValue()),int(self.input_maxbinOP1.GetValue()))
                Ny=H.arrayofbins(int(self.input_minbinOP2.GetValue()),int(self.input_maxbinOP2.GetValue()))

            else:

                Nx=H.arrayofbins(1,100)
                Ny=H.arrayofbins(1,100)                
        
            #print (int(self.input_minbinOP1.GetValue()),int(self.input_maxbinOP1.GetValue()))        
        
            Dx=H.arrayofbinwidths(H.x_max, H.x_min, Nx)
            Dy=H.arrayofbinwidths(H.y_max, H.y_min, Ny)
    
            H.matrixofbinwidths(Dx,Dy)
            
            H.cost_function(Nx,Ny)
        
        #Optimal Bin Size Selection    
        #-------------------------
            idx_min_Cxy=H.idx_min_cost_function()
    
            Cxymin=H.Cxy[idx_min_Cxy[0],idx_min_Cxy[1]] #value of the min Cxy

            if(sum(H.Cxy==Cxymin)==1): #test
                print "there is one min value"
            else:    
                print "there is not only one min value"

            optDxy=H.Dxy[idx_min_Cxy[0],idx_min_Cxy[1]]#get the bins size pairs that produces the minimum cost function

            optDx=optDxy[0]
            optDy=optDxy[1]

            idx_Nx=idx_min_Cxy[0]#get the index in x that produces the minimum cost function
            idy_Ny=idx_min_Cxy[1]#get the index in y that produces the minimum cost function

            bin1 = Nx[idx_Nx]
            bin2 = Ny[idy_Ny]
            
            print '# Cxy', Cxymin, "Bin_x",Nx[idx_Nx], optDx 
            print '# Cxy', Cxymin, "Bin_y",Ny[idy_Ny], optDy
    
            self.str_bestbin = ""    
            self.str_bestbin = "The bins used to optimize the FES were\nBin OP1 "+str(Nx[idx_Nx])+"\t\t\tBin OP2 "+str(Ny[idy_Ny])+"\n\n\n"         
    
        else: 

            bin1 = self.sc.GetValue()
            bin2 = self.sc2.GetValue()
            
            self.str_bestbin = "The bins used  OP1: "+str(bin1)+" OP2: "+str(bin2)+"\n\n\n"     
        
        
        temp = self.sc3.GetValue()
          

        if self.aut_manu_selec_fes=="":
            
            self.ShowMessage6()
        
        else:


            if self.aut_manu_selec_fes =='Automatic max and min':#The user does not select the max and min

                self.setting=self.fes( bin1, bin2 ,temp , None, None ,None,None)

            else:#The user selects the max and min

                imax1 = self.input_max1p3.GetValue()  # TERMINAR
                imin1 = self.input_min1p3.GetValue()
                imax2 = self.input_max2p3.GetValue()
                imin2 = self.input_min2p3.GetValue()
        
                self.setting=self.fes( bin1, bin2 ,temp ,imin1, imax1 ,imin2,imax2)



    def OnCompute_bw(self, e):
        """Execute the method to estimating the bin width"""
       
   
       
        if self.aut_manu_selec_btw=="":        
            self.ShowMessage8()
        else:

            self.plot_distributions()
            print "\n\n\nEstimating the bin width for each dataset.\n\n"


    def OnCompute_btn_points(self, e):
        """compute points ... """
        
        #self.cell_populated  #=[xedges[ai[0]], xedges[ai[0]+1], yedges[ai[1]], yedges[ai[1]+1] ]

        data_op1 = self.data_op1 #self.get_rg(self.path_rg)     
        data_op2 = self.data_op2 #self.get_rmsd(self.path_rmsd)

        input_data_x=[]
        input_data_y=[]
        input_data_time=[]
        count_c=0

        if self.str_selec =='Cell most populated':

            for i in range(len(data_op1)):
                if ( self.cell_populated[0] <= data_op1[i] <= self.cell_populated[1] and 
                     self.cell_populated[2] <= data_op2[i] <= self.cell_populated[3]):
                    count_c=count_c+1     
                    input_data_x.append(data_op1[i])            
                    input_data_y.append(data_op2[i])
                    
                    if self.data_time!=[]:
                        input_data_time.append(self.data_time[i])
        else:
            for i in range(len(data_op1)):

                if ( float(self.input_x1.GetValue()) <= data_op1[i] <= float(self.input_x2.GetValue()) and 
                     float(self.input_y1.GetValue()) <= data_op2[i] <= float(self.input_y2.GetValue())):
                    count_c=count_c+1
                    input_data_x.append(data_op1[i])            
                    input_data_y.append(data_op2[i])
                    
                    if self.data_time!=[]:
                        input_data_time.append(self.data_time[i])


        #print self.input_x1.GetValue(),self.input_x2.GetValue(),self.input_y1.GetValue(),self.input_y2.GetValue()

        pm=self.compute_point(input_data_x,input_data_y,met=self.str_selec1)

        dist2points= self.compute_dist2points(pm[0],pm[1],input_data_x,input_data_y,input_data_time)
        print dist2points

        str_output="The nearest points from the point that was computed\n\ndist\t\t\t\t\tx\t\t\ty\t\t\t\ttime"
        for l in range(self.sc4.GetValue()):
#            s="%\nf \t%f \t%f"% (dist2points[l][0],dist2points[l][1],dist2points[l][2])  #4.40958001e-06 	1.62743 	0.9995489 	295600.0
            if self.data_time!=[]:
                s="\n" +str(dist2points[l][0] )+" \t"+str(dist2points[l][1])+" \t" +str(dist2points[l][2])+" \t" +str(dist2points[l][3])
            else:                
                s="\n" +str(dist2points[l][0] )+" \t"+str(dist2points[l][1])+" \t" +str(dist2points[l][2])                
            str_output=str_output+ s
        self.outputbox3.SetValue("The region selected has "+str(count_c)+" points.\n" + str_output)
 






        #-------------------------------------plot

        matplotlib.rcParams['axes.unicode_minus'] = False
        fig, ax = plt.subplots()
        ax.plot(input_data_x,input_data_y, 'o')
        arrstyles = ['-', '->', '-[', '<-', '<->', 'fancy', 'simple','wedge']
        style=arrstyles[-2]
#        plt.annotate(self.str_selec1+"  \n" , xytext=(pm[0]+0.00001,pm[1]+0.00001), xy=(pm[0], pm[1]),arrowprops=dict(arrowstyle=style,facecolor='red'));

        plt.annotate(str("Point created from "+self.str_selec1+ " of x and y"), xytext=(-20,20), xy=(pm[0], pm[1]),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
        ax.plot(pm[0], pm[1], 'D')

        ax.set_title('Points inside the region selected')
        #plt.draw()
        plt.show()

        t="""
        from astroML.resample import bootstrap
        from astroML.stats import sigmaG,mean_sigma
        n = 100000  # number of bootstraps #stats.mode mean
        xc = bootstrap(input_data_x, n, np.mean, kwargs=dict(axis=-1))
        yc = bootstrap(input_data_y, n, np.mean, kwargs=dict(axis=-1))    
        ax.plot(xc,yc, 'o')
        ax.set_title('Points inside the region selected')
        plt.draw()
        plt.show()
        """




    def compute_hist_bins(self, style, data, name,  ax=None,mmin=None,mmax=None):
        """
         Compute a histogram and use of more sophisticated algorithms for determining bins.  
         Algorithms:
            'scott' - use Scott's rule to determine bins
            'freedman' - use the Freedman-diaconis rule to determine bins
        """

        if ax is not None:
            ax = plt.axes(ax)  

        if self.aut_manu_selec_btw =='Manual max and min':

            #Compute the number of bins, bins edges
            counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step', normed=False , range=(float(mmin), float(mmax)))  # IMP
            self.array_bins_edges.append(bins) #save the bins edges that were computed to be used to compute FES.
            ax.text(0.99, 0.95, '%s:\n%i bins' % (name, len(counts)), transform=ax.transAxes,ha='right', va='top', fontsize=12)

        else:      

            #Compute the number of bins, bins edges
            counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step', normed=False )  # IMP
            self.array_bins_edges.append(bins) #save the bins edges that were computed to be used to compute FES.
            ax.text(0.99, 0.95, '%s:\n%i bins' % (name, len(counts)), transform=ax.transAxes,ha='right', va='top', fontsize=12)

        ax.set_xlim(bins[0], bins[-1])

   
        return counts, bins #,ax


    
   
    
    def plot_distributions(self):
        """Plot four distributions with the bins estimated by Scott's and Freedman-diaconis rule."""

        #self.data_op1.max()        
        
        op1=self.data_op1
        op2=self.data_op2

        data1_min = op1.min()
        data1_max = op1.max()
        data2_min = op2.min()
        data2_max = op2.max()
      
        #print   "test "+ data1_min
        
        # Plot results
        fig = plt.figure(figsize=(10, 5))
        fig.subplots_adjust(hspace=0.1, left=0.1, right=0.95, wspace=0.1)
        ax = [fig.add_subplot(2, 2, i + 1) for i in range(4)]
        
        self.array_bins_edges=[]

        array_info_hist=[]   
                               
        # first column: data1 
        array_info_hist.append(  #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('scotts', op1, 'Scott\'s Rule', ax=ax[0],mmin=self.input_min1p2.GetValue(), mmax=self.input_max1p2.GetValue()), #first elem. of array
                        self.name_data_op1,'Scott\'s Rule',data1_min, data1_max #others elements
                ])

        array_info_hist.append( #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('freedman', op1, 'Freedman-Diaconis', ax=ax[2],mmin=self.input_min1p2.GetValue(), mmax=self.input_max1p2.GetValue()), #first elem. of array
                        self.name_data_op1,  'Freedman-Diaconis',data1_min, data1_max #others elements
                ])
       
        ax[0].set_title('Distribution '+self.name_data_op1)
        ax[0].set_ylabel('Count')
        ax[2].set_ylabel('Count')
        ax[2].set_xlabel(self.name_data_op1)
        
        # second column: data2
        array_info_hist.append( #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('scotts', op2, 'Scott\'s Rule', ax=ax[1], mmin=self.input_min2p2.GetValue(),mmax=self.input_max2p2.GetValue()), #first elem. of array
                        self.name_data_op2,'Scott\'s Rule',data2_min, data2_max #others elements
                ])

        array_info_hist.append( #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('freedman', op2, 'Freedman-Diaconis', ax=ax[3], mmin=self.input_min2p2.GetValue(),mmax=self.input_max2p2.GetValue()), #first elem. of array
                       self.name_data_op2,'Freedman-Diaconis',data2_min, data2_max #others elements
                ])
        
        ax[1].set_title('Distribution '+self.name_data_op2)
        ax[3].set_xlabel(self.name_data_op2)

        plt.show()


        self.str_output_hist_inf=""  #send the information about the distribution to outputbox1
        for i in array_info_hist:
            self.str_output_hist_inf = self.str_output_hist_inf + ('data: %s\n%s: %i bins \nHistogram\nmin: %f\nmax: %f\nData\nmin: %f\nmax: %f\n\n' % 
                                  (i[1],i[2], len(i[0][0]), (i[0][1][0]) ,(i[0][1][-1]) ,float(i[3]),float(i[4]) )) 
            
        for i in array_info_hist:                                  
            #put this information in log file
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\n" + i[2] +" rule to determine bins to " + i[1] +" data." 
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\nNumber of bins: "+ str(len(i[0][0]))+ " and number of bin edges: "+  str(len(i[0][1]))
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\nCounts in each bin: " 
            for j in i[0][0]:
                self.str_output_hist_inf= self.str_output_hist_inf+ "\n"+str(j)
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\nBin edges: "
            for j in i[0][1]:
                self.str_output_hist_inf= self.str_output_hist_inf+ "\n"+str(j)


        self.outputbox1.SetValue(self.str_output_hist_inf)




    def fes(self,_i1,_i2 , _temp , data1_min=None,data1_max=None,data2_min=None,data2_max=None):
        """Calculate FES and produces a output that has rmsd, rg and Dg from rmsd.xvg and rg.xvg"""	
    
        print "\n\nComputting the FES\n\n"
        self.fes_computed = True

        #open the input files

        if (data1_min==None or data1_max==None or data2_min==None or data2_max==None):
        #get the min and max ->  IMP - refactor -IMP
            data1_min = self.data_op1.min()
            data1_max = self.data_op1.max()
            data2_min = self.data_op2.min()
            data2_max = self.data_op2.max()

        #set the min and max    ->  IMP - refactor -IMP
            minv1 = float(data1_min)
            maxv1 = float(data1_max)
            minv2 = float(data2_min)
            maxv2 = float(data2_max)


        else:

            minv1 = float(data1_min)
            maxv1 = float(data1_max)
            minv2 = float(data2_min)
            maxv2 = float(data2_max)

        extent = [minv1, maxv1, minv2, maxv2 ]

        #save a file with two columns: RG and RMSD    
        #np.savetxt('rg_rmsd.out', np.array([data_rg['rg'],data_rmsd['rmsd']] ).transpose(), fmt='%f %f', delimiter=',')


        #bins
        i1 = int(_i1)
        i2 = int(_i2)
    
        V = np.zeros((i1,i2))
        DG = np.zeros((i1,i2))

        I1 = maxv1 - minv1
        I2 = maxv2 - minv2

        kB = 3.2976268E-24
        An = 6.02214179E23
        T = float(_temp) #temperature


        #old code to do the count
        x=self.data_op1
        y=self.data_op2
        r=[[minv1, maxv1], [minv2, maxv2]]


        #New code to do the count. It use the bins width that were got by rules or not    

        #OBS.: A two dimensional histogram consists of a set of bins which count the number of events falling in a given area of the (x,y) plane. 
        if(len(self.array_bins_edges)==4):
            if(i1==(len(self.array_bins_edges[0])-1)  and i2 ==(len(self.array_bins_edges[2])-1) ):
                print 1,"use the bins width got by rules"
                H, xedges, yedges = np.histogram2d(x, y ,bins=[self.array_bins_edges[0],self.array_bins_edges[2]] ,range=r, normed=False)#bins=(i1, i2))
            elif (i1==(len(self.array_bins_edges[1])-1)  and i2 ==(len(self.array_bins_edges[3])-1) ): 
                print 2,"use the bins width got by rules"
                H, xedges, yedges = np.histogram2d(x, y, bins=[self.array_bins_edges[1],self.array_bins_edges[3]] , range=r, normed=False )#bins=(i1, i2))
            else:     #don't use the bins width got by rules
                print 3,"don't use the bins width got by rules"
                H, xedges, yedges = np.histogram2d(x, y ,bins=(i1, i2),range=r, normed=False)
        else:         #don't use the bins width gotting by rules

            H, xedges, yedges = np.histogram2d(x, y ,bins=(i1, i2), range=r)
            print 4,"don't use the bins width got by rules" #,H, xedges, yedges
		
        # Finding the maximum   #I can use this ->  np.where(x == np.max(x))     
        P = list()
        for x in range(H.shape[0]):
    	    for y in range(H.shape[1]):
    	    	P.append(H[x][y])
                
        Pmax = max(P)



        LnPmax = math.log(Pmax) 

        print "\n\nbins that were used: ",H.shape
	
        #Compute Gibbs free energy landscapes
        for x in range(H.shape[0]):
            for y in range(H.shape[1]):
    	        if H[x][y] == 0:
    		        DG[x][y] = 10
            		continue
            	else:
    		        DG[x][y] = -0.001*An*kB*T*(math.log(H[x][y])-LnPmax)


        #test about cell with the most number of point
        ai=np.where(H == np.max(H))
        #print np.where(H == np.max(H)),H[np.where(H == np.max(H))],Pmax,np.where(DG == np.min(DG)),DG[np.where(DG == np.min(DG))]
        count_c=0

        self.cell_populated=[xedges[ai[0]], xedges[ai[0]+1], yedges[ai[1]], yedges[ai[1]+1] ]

        for i in range(len(self.data_op1)):
            if ( xedges[ai[0]]<= self.data_op1[i]   <=xedges[ai[0]+1]     and    yedges[ai[1]]<=   self.data_op2[i]  <=yedges[ai[1]+1]):
                count_c=count_c+1
    
        str_output=  "The cell with the highest population\n has "+str(count_c)+" points and coordinates \n x1: "+ str(xedges[ai[0]]) +" x2: " +str(xedges[ai[0]+1]) + "\n y1: "+ str(yedges[ai[1]]) +" y2: " + str(yedges[ai[1]+1]) 
      
        print str_output
        
        #compute the structure more "stable" in the cell of the least free energy
        self.outputbox2.SetValue(self.str_bestbin+str_output)
               
        #return  (outfilename,minv1, maxv1, minv2, maxv2, fontsize_tick)
        return  (minv1, maxv1, minv2, maxv2, H,DG,xedges,yedges ,extent)
        





    def write_fes_file(self,outfilename,H,DG,xedges,yedges):
        """
        Produces the output file with three columns rg, rmsd, DG.
        I was using the media of the each edges correspondet to each DG.  
        """
        
        ofile = open(outfilename,'w')    # open file for writing

        for x in range(H.shape[0]):
        	for y in range(H.shape[1]):
        		#ofile.write(str((xedges[x]+xedges[x+1])/2.) + "\t" + str((yedges[y]+yedges[y+1])/2.) + "\t" + str(DG[x][y])+"\n")
        		ofile.write(str(xedges[x]) + "\t" + str(yedges[y]) + "\t" + str(DG[x][y])+"\n")    		
        	ofile.write("\n")
         
        ofile.close()




    def compute_point(self,datax,datay,met="mean"):
        "Compute the mean point from data in x and y"

        if met=="mean":
            print "mean"
            x=np.mean(datax)
            y=np.mean(datay)

        if met=="median":
            print "median"
            x=np.median(datax)
            y=np.median(datay)

        if met=="mode":
            print "mode"
            x=stats.mode(datax)
            y=stats.mode(datay)
            x=x[0][0]
            y=y[0][0]

        return (x,y)


    def compute_dist2points(self,pmx,pmy,datax,datay,datatime):
        "compute the distance betwen mean point and all points"
    
        distpoint=[]
        
        for i in range(len(datax)):

            if self.data_time!=[]:
                p = ( float(sqrt((pmx-datax[i])**2) + ((pmy-datay[i])**2)) , float(datax[i]) ,float(datay[i]),float(datatime[i]) )    
            else:
                p = ( float(sqrt((pmx-datax[i])**2) + ((pmy-datay[i])**2)) , float(datax[i]) ,float(datay[i]) )    
            distpoint.append(p)        
            print p
        print "\n\nsort the points with the least distance from mean point.\n\n"      
        
        if self.data_time!=[]:        
            distAB = np.array( distpoint , dtype=[('dist', float),('x', float), ('y', float),('time', float)])        
        else:
            distAB = np.array( distpoint , dtype=[('dist', float),('x', float), ('y', float)])        
            
        distAB.sort(order='dist')   #sort the points with the least distance from mean point      
        return distAB



    def plot_fes_hist2d(self,extent,DG):
        

        #---------------------------- criar um funcao fora remove------------------------------------------------------  
        #create a method to produce this plots      
        Hmasked = np.ma.masked_where(DG==10,DG)

        #for i in range(50):

        plt.imshow( Hmasked.T,extent=extent,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
        plt.colorbar().set_label(r'$\Delta G(kcal/mol)$')   
        plt.xlabel(self.name_data_op1) 
        plt.ylabel(self.name_data_op2)
        plt.title(str(self.output_title_fes.GetValue())) #self.output_title.GetLabel()
    
#        plt.set_label('$\Delta G(Kcal/mol)$',fontsize=17)
#        plt.colorbar()
        plt.show()

        #    plt.savefig("filename"+str(i), dpi=100)
        #    plt.clf()

        #for i in range(50):
        
        #plt.imshow(Hmasked.T,extent=extent,interpolation='nearest',origin='lower',aspect='auto')
        #plt.colorbar()
        #plt.show()

        #    plt.savefig("filename"+str(i+50), dpi=100)
        #    plt.clf()

        #for i in range(50):

        #plt.imshow(Hmasked.T,extent=extent,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.gnuplot)
        #plt.colorbar()
        #plt.show()

        #    plt.savefig("filename"+str(i+100), dpi=100)
        #    plt.clf()

        #os.system('mencoder mf://*.png -mf type=png:w=800:h=600:fps=25 -ovc lavc -lavcopts vcodec=mpeg4 -oac copy -o output.avi')


        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


    def plot_fes_Contour(self,colorscale,outfilename,data1_min,data1_max,data2_min,data2_max, fontsize_tick):
        """Produces contour plot"""

        data_fes = genfromtxt(outfilename, #skip_header=22, 
                        dtype={'names':['rg','rmsd','Dg'],
                       'formats':[float,float,float]})
        # make up data.
        #npts = int(raw_input('enter # of random points to plot:'))
        npts = 200 #  np.shape(data_fes['rg'])[0]

        x = data_fes['rg']
        y = data_fes['rmsd']
        z = data_fes['Dg']


     
        # define grid.
        xi = np.linspace(data1_min,data1_max,npts)
        yi = np.linspace(data2_min,data2_max,npts)
        # grid the data.
        #zi = griddata(x,y,z,xi,yi,interp='linear') #problem with interp='linear'
        zi = griddata(x,y,z,xi,yi)
        # contour the gridded data, plotting dots at the nonuniform data points.
        CS = plt.contour(xi,yi,zi,10,linewidths=0.001,colors='k')
        
        # rainbow |

        CS = plt.contourf(xi,yi,zi,100,cmap=colorscale,
                      vmax=abs(zi).max(), vmin=-abs(zi).max())


        cb=plt.colorbar(orientation="vertical")
        cb.set_label('$\Delta G(kcal/mol)$',fontsize=17)
        plt.xlabel(self.name_data_op1)
        plt.ylabel(self.name_data_op2)
        # plot data points.
        #plt.scatter(x,y,marker='o',c='b',s=5,zorder=10)
        plt.xlim(data1_min,data1_max)
        plt.ylim(data2_min,data2_max)

        override = {
       'fontsize'            : 50,
       'verticalalignment'   : 'center',
       'horizontalalignment' : 'right'}
    
        plt.yticks( fontsize=fontsize_tick )    
        plt.xticks( fontsize=fontsize_tick )
        #plt.ylabel("RMSD",override)
        #plt.xlabel("RMSD",override)

        plt.title(str(self.output_title_fes.GetValue()))
        plt.show()











                      
def main():
    
    ex = wx.App()
    Big_house_tools(None)
    ex.MainLoop()

    

if __name__ == '__main__':
    main()  


















