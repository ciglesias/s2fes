# BigHouse's tools - README
Cristovao Freitas Iglesias Junior, LMDM/IBCCF/UFRJ/Brazil, Copyright 2019
#
#


Bighouse's tools (BHT) 
-------------------------

	Big-house’s Tools (BHT) is a software with a graphical interface that provides a set of tools to perform 
	analysis on Molecular Dynamics simulation (MD) results .
	BHT is an open-source software with the objective: (1) gathers in one place useful tools to analyse the 
	results of the MD process and (2) allows the use of these tools through an intuitive graphical interface 
	avoiding the use of the command line.
	The software BHT can be seen like a platform totally free where the developers and/or 
	researchers can put their tools and integrates them so other researchers they can be used through a graphical interface.
	Thus aiming to facilitate and increase the efficiency of the analysis results of the MD process. 
	The first tools available on BHT software are: Optimization of 1D and 2D histograms and 
	surface free energy (free energy surface - FES).





FES
-------

    Free energy surface (FES) is a analyse available in BGT. Fes is a surface 
    computed by Gibbs free energy surface by Bolzmann equation,
                                     ∆G=-kT[ln(P)-ln(Pmax)]
    that allow explore the wide spectrum of conformations that biological 
    macro-molecules (proteins) typically can access. For example, the FES allow 
    know what were the most stable conformation in a sample of molecular dynamics.
    In Gibbs equation P is the probability distribution of the molecular system along 
    some (in general multidimensional) coordinate r (order parameter and Pmax denotes 
    its maximum, which is subtracted to ensure that ∆G=0 for the lowest free energy 
    minimum. Popular choices for the coordinate r include the fraction of native contacts, 
    the radius of gyration, and the root mean square deviation of the molecule with 
    respect to the native state. The probability distribution along these “order parameters” 
    may be obtained from experiment, from a theoretical model, or from a computer 
    simulation like molecular dynamics. The resulting free energy “landscape” has promoted 
    much of the recent progress in understanding protein folding.



Required packages to run this software
-------------------

    wx
        command line: sudo apt-get install python-wxgtk2.8 python-wxtools wx2.8-doc wx2.8-examples wx2.8-headers wx2.8-i18n
                      sudo apt-get install python-wxgtk3.0  python-wxtools  wx3.0-doc  wx3.0-examples  wx3.0-headers wx3.0-i18n


    numpy
        command line: sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose

    astroml 
        https://pypi.python.org/pypi/astroML/
        http://www.astroml.org/

    scikit-learn
        https://pypi.python.org/pypi/scikit-learn/0.14.1





Installation 
------------

	The "installation" of the BT is very simple!
    Open the terminal and execute the command below:


	python INSTALL.py


	You only have to run the python file 'INSTALL.py' in the 
    same folder that 'big_house_tools.py' , 'optHist2d_OO.py'
    and 'bht_icon.png'. It will create a icon in your desktop 
    for you to execute the software easily from there, only 
    clicking on the icon. 

	





	













