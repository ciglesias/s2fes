from numpy import array, arange, argmin, sum, mean, var, size, zeros,	where, histogram
from numpy.random import normal
from matplotlib.pyplot import figure, plot, hist, bar, xlabel, ylabel,title, show, savefig
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize
from past.builtins import xrange
import pandas as pd
import time


#https://www2.hawaii.edu/~jonghyun/classes/S18/CEE696/files/14_global_optimization.pdf
# https://perso.crans.org/besson/publis/notebooks/Simulated_annealing_in_Python.html
# https://pablormier.github.io/2017/09/05/a-tutorial-on-differential-evolution-with-python/
# https://matplotlib.org/3.2.1/gallery/index.html
# https://plotly.com/python/2D-Histogram/#2d-histogram-overlaid-with-a-scatter-chart
# https://stackoverflow.com/questions/27156381/python-creating-a-2d-histogram-from-a-numpy-matrix


class Opthist2d():


    def __init__(self,data_x,data_y):

        self.data_x=data_x
        self.data_y=data_y
        self.x_max = max(data_x)
        self.x_min = min(data_x)
        self.y_max = max(data_y)
        self.y_min = min(data_y)
        self.Nx_MIN = 1   #Minimum number of bins in x (integer)
        self.Nx_MAX = 100  #Maximum number of bins in x (integer)
        self.Ny_MIN = 1 #Minimum number of bins in y (integer)
        self.Ny_MAX = 100  #Maximum number of bins in y (integer)
        self.Dxy=[]
        self.Cxy=[]
        self.Cxy_Dxy_plot =[] #save data to plot in scatterplot x,y,z


    def sample_resolution(self,data):
        """Compute and return the sample resolution of some data"""
        buf = abs(np.diff(np.sort(data)))
        dx = np.min(buf[buf != 0])
        return dx

    def binwidth(self,datamax,datamin,samp_resol):
        """Return a binwidth"""
        N_max =  (datamax-datamin) / float(samp_resol)
        return N_max

    def large_binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / samp_resol/ 2
        return int(N_max)

    def arrayofbins(self,N_MIN,N_MAX):
        """Return a array of bins """
        return arange(N_MIN, N_MAX) # #of Bins

    def arrayofbinwidths(self,datamax,datamin, N):
        """Return a array of binwidths"""
        return (datamax - datamin) / N  #Bin size vector

    def matrixofbinwidths(self,Dx,Dy):
        """Set the matrix of binwidths and save in self.Dxy"""
        for i in Dx:    #Bin size vector
            a=[]
            for j in Dy:    #Bin size vector
                a.append((i,j))
            self.Dxy.append(a)
        self.Dxy=array(self.Dxy, dtype=[('x', float),('y', float)]) #matrix of bin size vector
        return self.Dxy


    def cost_function_histogram2d(self,N):
        """Shimazaki and Shinomoto Cost function to find the best Bins pairs to histogram2d"""
        Nx,Ny=N
        Nx=max(Nx, 1)
        Ny=max(Ny, 1)
        Nx=int(Nx)
        Ny=int(Ny)
        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        return Cxy

        

    def compute_cost_function_h2d_with_DA(self,bounds):
        """ Compute the cost function for hist2d using  dual_annealing and return the best bins pairs"""
        print("Runnig dual_annealing ...\n")
        #bounds = [(1, Nx_max), (1, Ny_max)]
        start_time = time.time()
        res_dual_annealing = optimize.dual_annealing(self.cost_function_histogram2d, bounds,maxiter=10000,no_local_search="True")
        time_DA="%s" % (time.time() - start_time)
        print("_____dual_annealing results_____\n",res_dual_annealing)
        print("Total time: ",time_DA)
        return [int(res_dual_annealing['x'][0]),int(res_dual_annealing['x'][1])]

    def compute_cost_function_h2d_with_DE(self,bounds):
        """ Compute the cost function for hist2d using  differential_evolution and return the best bins pairs"""
        print("Runnig Differential_evolution ...\n")
        # bounds = [(1, Nx_max), (1, Ny_max)]
        max_interaction=10000
        start_time = time.time()
        res_differential_evolution = optimize.differential_evolution(self.cost_function_histogram2d, bounds, strategy='best1bin',
                maxiter=max_interaction,popsize=4000, tol=0.01, mutation=(0,1),recombination=0.7,
                seed=None,callback=None, disp=False, polish=True,init='latinhypercube', atol=0)
        time_DE="%s" % (time.time() - start_time)
        print("_____differential_evolution_____\n",res_differential_evolution)
        print("Total time: ",time_DE)
        return  [int(res_differential_evolution['x'][0]),int(res_differential_evolution['x'][1])]

    def compute_cost_function_h2d_with_BF(self,ranges):
        """ Compute the cost function for hist2d using  differential_evolution and return the best bins pairs"""
        #ranges=(slice(1, 100,1),slice(1, 100,1))
        print("brute force runnig...")
        start_time = time.time()
        res_brute=optimize.brute(self.cost_function_histogram2d,ranges, disp=True, finish=None)
        time_BF="%s" % (time.time() - start_time)
        print("_____res_brute_____\n",res_brute)
        print("Total time: ",time_BF)
        return  [int(res_brute[0]),int(res_brute[1])]

    def cost_function_hist1d_x(self,N):
        """ Compute the cost function for hist1d using brute force """
        Nx=N[0]
        Nx=max(Nx, 1)
        Nx=int(Nx)
        Dx=(self.x_max - self.x_min)/float(Nx)
        ki = histogram(self.data_x, bins=Nx)
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cx = (2 * k - v) / ( (Dx)**2 )  #The cost Function
        #print(Cx,Dx,Nx,self.x_max,self.x_min)
        return Cx


    def cost_function_hist1d_y(self,N):
        """ Compute the cost function for hist1d using brute force """
        Ny=N[0]
        Ny=max(Ny, 1)
        Ny=int(Ny)
        Dy=(self.y_max - self.y_min)/float(Ny)
        ki = histogram(self.data_y, bins=Ny)
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cy = (2 * k - v) / ( (Dy)**2 )  #The cost Function
        return Cy


    def idx_min_cost_function(self):
        """return the index of combination of i and j that produces the minimum cost function"""
        idx_min_Cxy=np.where(self.Cxy == np.min(self.Cxy)) #get the index of the min Cxy
        return (idx_min_Cxy[0][0],idx_min_Cxy[1][0])


def main():
    #COMPUTE THE COST FUNCTION
    #-------------------------
    from numpy import genfromtxt
    data_rg = genfromtxt("test_codes_hopt/bid-rg-idx-3.xvg"
                            ,skip_header=22,
                            dtype={'names':['time', 'rg','rgx','rgy','rgz'],
                           'formats':[int,float,float,float,float]})
    data_rmsd = genfromtxt("test_codes_hopt/bid-rmsd-idx-46_3.xvg",skip_header=13,
                            dtype={'names':['time', 'rmsd'],
                           'formats':[int,float]})
    y=data_rmsd['rmsd']
    x=data_rg['rg']

    H=Opthist2d(x,y)

    dx=H.sample_resolution(H.data_x)
    dy=H.sample_resolution(H.data_y)
    print("sample_resolution of X and Y: ",dx,dy)

    Nx_max=H.large_binwidth(H.x_max, H.x_min,dx)
    Ny_max=H.large_binwidth(H.y_max, H.y_min,dy)
    print("large_binwidth of X and Y: ",Nx_max,Ny_max)

    Nx_max=1000
    Ny_max=1000

    ranges=(slice(1, 100,1),slice(1, 100,1))
    print(Opthist2d(x,y).compute_cost_function_h2d_with_BF(ranges))

    bounds = [(1, 1000), (1, 1000)]
    print(Opthist2d(x,y).compute_cost_function_h2d_with_DA(bounds))

    bounds = [(1, 1000), (1, 1000)]
    print(Opthist2d(x,y).compute_cost_function_h2d_with_DE(bounds))


if __name__ == '__main__':
    main()
