#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
updated on April 2, 2020 in Ottawa - CA, during the global outbreak of COVID-19 a pandemic.
@author: Cristovao Iglesias
'''


from matplotlib.pyplot import figure, plot, hist, bar, xlabel, ylabel,title, show, savefig

from scipy.cluster.hierarchy import dendrogram
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from numpy import genfromtxt
import sys,string, os
import numpy as np
import math
from numpy.random import uniform, seed
#from matplotlib.mlab import griddata #https://stackoverflow.com/questions/56601129/how-to-install-and-use-pandastable-griddata-missing
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from pylab import *
#from matplotlib import *
from matplotlib.patches import Patch
from optparse import OptionParser
import wx
from scipy import stats
#from astroML.plotting import hist
from astropy.visualization import hist
from optHist2d_OO import Opthist2d
from scipy import optimize


#from compute_hist_bin_width import compute_hist_bins



class Big_house_tools(wx.Frame):
    """A set of tools to analyse molecular dynamics results."""

    def __init__(self, *args, **kw):
        super(Big_house_tools, self).__init__(*args, **kw)

        self.InitUI()

        #path to the files of the rmsd and rg
        self.path_rmsd=""
        self.path_rg=""
        self.path_op1=""
        self.path_op2=""
        self.path_op1op2=""
        self.array_bins_edges=[]
        self.str_selec=""
        self.str_selec1=""
        self.type_file_selec_op1=""
        self.type_file_selec_op2=""
        self.type_file_selec_op1op2=""
        self.aut_manu_selec_fes=""
        self.aut_manu_selec_btw=""
        self.cells_energy_minimum=[]
        self.data_op1=[]            #save the data of order parameter 1
        self.data_op2=[]            #save the data of order parameter 2
        self.data_time=[]
        # THIS IS FOR TESTS
        # self.data_op1=self.get_rmsd('/Users/cristovao/Devel/project_S2FES/s2fes/dataset_test/bid-rmsd-idx-46_3.xvg')['rmsd']
        # self.data_time=self.get_rmsd('/Users/cristovao/Devel/project_S2FES/s2fes/dataset_test/bid-rmsd-idx-46_3.xvg')['time']
        # self.data_op2=self.get_rg('/Users/cristovao/Devel/project_S2FES/s2fes/dataset_test/bid-rg-idx-3.xvg')['rg']

        self.name_data_op1=""
        self.name_data_op2=""
        self.str_output_hist_inf=""
        self.export_fes_file=False
        self.plot_fes = False
        self.fes_computed = False
        self.setting=[] #(minv1, maxv1, minv2, maxv2, H,DG,xedges,yedges ,extent)
        self.output_path_fesfile=""
        self.aut_manu_selec_bins=""
        self.str_bestbin =""
        #self.button1;



    def InitUI(self):

        #pnl = wx.Panel(self)

        self.notebook_1 = wx.Notebook(self, -1, style=0)
        self.notebook_1_pane_1 = wx.Panel(self.notebook_1, -1)
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, -1)
        self.notebook_1_pane_3 = wx.Panel(self.notebook_1, -1)
        self.notebook_1_pane_4 = wx.Panel(self.notebook_1, -1)

        #self.button_1 = wx.Button(self.notebook_1_pane_1, -1, "button_1")

        self.__set_properties()
        self.__do_layout()

        #---------------------------------- labels ------------------------------------

        #Pane1
        #self.filenamerg = wx.StaticText(self.notebook_1_pane_1, label='RG file (input): ', pos=(250, 115))
        #self.filenamermsd = wx.StaticText(self.notebook_1_pane_1, label='RMSD file (input): ', pos=(250, 145))
        wx.StaticText(self.notebook_1_pane_1, label='', pos=(20,20))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Files', pos=(400, 85))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Type file', pos=(200, 85))
        #self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='CSV file with two column', pos=(180, 180))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Order Parameter name (OP1)', pos=(675, 110))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='Order Parameter name (OP2)', pos=(675, 140))
        self.filenamergxx = wx.StaticText(self.notebook_1_pane_1, label='OP1 name             OP2 name', pos=(570, 200), size=(300,50))

        bitmap = wx.Bitmap("labnet.png", wx.BITMAP_TYPE_ANY)
        image = wx.ImageFromBitmap(bitmap)
        image = image.Scale(100, 50, wx.IMAGE_QUALITY_HIGH)
        resultbitmap = wx.BitmapFromImage(image)

        self.log_labnet = wx.StaticBitmap(self.notebook_1_pane_1, wx.ID_ANY, resultbitmap)
        self.log_labnet.SetPosition((700, 300))


        #Pane2
        wx.StaticText(self.notebook_1_pane_2, label='Max and Min Values', pos=(450,20), size=(300,50))
        #self.distr_info=wx.StaticText(self.notebook_1_pane_2, label='Information', pos=(20,690))
        #wx.StaticText(self.notebook_1_pane_2, label='Output file name: ', pos=(550, 20))
        self.max1p2=wx.StaticText(self.notebook_1_pane_2, label='OP1 max: ', pos=(450, 90))
        self.min1p2=wx.StaticText(self.notebook_1_pane_2, label='OP1 min: ', pos=(450, 120))
        self.max2p2=wx.StaticText(self.notebook_1_pane_2, label='OP2 max: ', pos=(450, 150))
        self.min2p2=wx.StaticText(self.notebook_1_pane_2, label='OP2 min: ', pos=(450, 180))

        #Pane3
        # self.max1p3 =wx.StaticText(self.notebook_1_pane_3, label=' Runnig ...', pos=(130, 260), size=(300,50))
        # self.max1p3.Hide()
        wx.StaticText(self.notebook_1_pane_3, label='Bins for OP1 and OP2', pos=(20,20), size=(300,20))


        self.label_nbinsop1=wx.StaticText(self.notebook_1_pane_3, label='Number of bins to OP1\nmin:\t\t\tmax:', pos=(20,80), size=(300,50))
        self.label_nbinsop2=wx.StaticText(self.notebook_1_pane_3, label='Number of bins to OP2\nmin:\t\t\tmax:', pos=(20,145), size=(300,50))
        self.label_searchbins=wx.StaticText(self.notebook_1_pane_3, label='Searching for bins\nbetween 1 and 1000 for OP1\nand between 1 and 1000 for OP2.', pos=(20,80), size=(300,50))
        self.label_op1bin=wx.StaticText(self.notebook_1_pane_3, label='OP1 Bins: ', pos=(20, 80))
        self.label_op2bin=wx.StaticText(self.notebook_1_pane_3, label='OP2 Bins: ', pos=(20, 105))
        wx.StaticText(self.notebook_1_pane_3, label='Temperature (K): ', pos=(50, 210))

        self.label_cfilename=wx.StaticText(self.notebook_1_pane_3, label='Output file name', pos=(670, 55))
        self.label_cfolder=wx.StaticText(self.notebook_1_pane_3, label='Choose one folder', pos=(670, 85))
        self.pt_fes=wx.StaticText(self.notebook_1_pane_3, label='Plot title: ', pos=(550, 180)) #input Plot title
        # self.max1p3= wx.StaticText(self.notebook_1_pane_3, label='OP1 max: ', pos=(300, 90))
        # self.min1p3= wx.StaticText(self.notebook_1_pane_3, label='OP1 min: ', pos=(300, 120))
        # self.max2p3= wx.StaticText(self.notebook_1_pane_3, label='OP2 max: ', pos=(300, 150))
        # self.min2p3= wx.StaticText(self.notebook_1_pane_3, label='OP2 min: ', pos=(300, 180))

        #Pane4

        wx.StaticText(self.notebook_1_pane_4, label='Coordinates', pos=(300,20))
        wx.StaticText(self.notebook_1_pane_4, label='Point', pos=(30,20))
        wx.StaticText(self.notebook_1_pane_4, label='Nº output points: ', pos=(20, 105))
        #wx.StaticText(self.notebook_1_pane_4, label='Output file name: ', pos=(550, 20))
        #wx.StaticText(self.notebook_1_pane_4, label='Plot title: ', pos=(550, 180))
        self.x1 = wx.StaticText(self.notebook_1_pane_4, label='x1: ', pos=(300, 90)) #input cell coordinates
        self.x2 = wx.StaticText(self.notebook_1_pane_4, label='x2: ', pos=(300, 120))
        self.y1 = wx.StaticText(self.notebook_1_pane_4, label='y1: ', pos=(300, 150))
        self.y2 = wx.StaticText(self.notebook_1_pane_4, label='y2: ', pos=(300, 180))



        #---------------------------------- output ------------------------------------
        self.outputbox1 = wx.TextCtrl(self.notebook_1_pane_2, style=wx.TE_MULTILINE, size=(400,590), pos=(30, 90))
        self.outputbox2 = wx.TextCtrl(self.notebook_1_pane_3, style=wx.TE_MULTILINE, size=(450,200), pos=(60, 300))
        self.outputbox3 = wx.TextCtrl(self.notebook_1_pane_4, style=wx.TE_MULTILINE, size=(550,390), pos=(30, 260))


        #---------------------------------- input ------------------------------------

        distros = ['choose','Manual max and min', 'Automatic max and min']
        options1 = ['choose','Cell most populated', 'Region']
        options2 = ['choose','mean','mode','median','centroid']
        options3 = ['choose','gromacs rmsd', 'gromacs rg', 'gromacs PCA - proj1D', 'only one column' ]
        options4 = ['choose','gromacs PCA - proj2D', 'CSV file with two column' ]
        options5 = [ 'choose','Automatic Prediction of Bins','Automatic Prediction of Bins From Specific Range','Specific Bins' ]

        cb  = wx.Choice(self.notebook_1_pane_2,  pos=(450, 40),  choices=distros )
        #cb2 = wx.Choice(self.notebook_1_pane_3,  pos=(330, 40),  choices=distros )
        cb3 = wx.Choice(self.notebook_1_pane_4,  pos=(300, 40),  choices=options1 )
        cb4 = wx.Choice(self.notebook_1_pane_4,  pos=(30, 40),   choices=options2 )
        cb5 = wx.Choice(self.notebook_1_pane_1,  pos=(120, 110), choices=options3 )
        cb6 = wx.Choice(self.notebook_1_pane_1,  pos=(120, 140), choices=options3 )
        cb7 = wx.Choice(self.notebook_1_pane_1,  pos=(120, 170), choices=options4 )
        cb8 = wx.Choice(self.notebook_1_pane_3,  pos=(20, 40),  choices=options5 )


        self.sc = wx.SpinCtrl(self.notebook_1_pane_3, value='0', pos=(150, 75), size=(60, -1)) #bin rg
        self.sc.SetRange(-459, 1000)
        self.sc2 = wx.SpinCtrl(self.notebook_1_pane_3, value='0', pos=(150, 100), size=(60, -1)) #bin rmsd
        self.sc2.SetRange(-459, 1000)
        self.sc3 = wx.SpinCtrl(self.notebook_1_pane_3, value='0', pos=(160, 210), size=(60, -1)) #temperatura
        self.sc3.SetRange(-459, 1000)

        self.sc4 = wx.SpinCtrl(self.notebook_1_pane_4, value='0', pos=(150, 100), size=(60, -1)) #'Nº output points: '
        self.sc4.SetRange(1, 20000)

        self.output_filename_fes = wx.TextCtrl(self.notebook_1_pane_3, pos=(550, 50), size=(120, -1))  #output name
        #self.output_bw = wx.TextCtrl(self.notebook_1_pane_2, pos=(550, 40), size=(120, -1))  #output name
        #self.output_bw = wx.TextCtrl(self.notebook_1_pane_4, pos=(550, 40), size=(120, -1))  #output name

        self.op1_name_onecol = wx.TextCtrl(self.notebook_1_pane_1, pos=(530, 110), size=(120, -1))  # op1 name
        self.op2_name_onecol = wx.TextCtrl(self.notebook_1_pane_1, pos=(530, 140), size=(120, -1))  # op2 name
        self.op1_name_twocol = wx.TextCtrl(self.notebook_1_pane_1, pos=(530, 170), size=(120, -1))  # op1 name
        self.op2_name_twocol = wx.TextCtrl(self.notebook_1_pane_1, pos=(650, 170), size=(120, -1))  # op2 name

        self.output_title_fes = wx.TextCtrl(self.notebook_1_pane_3, pos=(550, 200), size=(120, -1))  #plot title name
        #self.output_title = wx.TextCtrl(self.notebook_1_pane_4, pos=(550, 200), size=(120, -1))  #plot title name

        # self.input_max1p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 90), size=(60, -1))  #input max
        # self.input_min1p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 120), size=(60, -1))  #input min
        # self.input_max2p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 150), size=(60, -1))  #input max
        # self.input_min2p3 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(380, 180), size=(60, -1))  #input min


        # self.label_nbinsop1=wx.StaticText(self.notebook_1_pane_3, label='Number of bins to OP1\nmin:\t\t\tmax:', pos=(20,80), size=(300,50))
        # self.label_nbinsop2=wx.StaticText(self.notebook_1_pane_3, label='Number of bins to OP2\nmin:\t\t\tmax:', pos=(20,145), size=(300,50))

        self.input_maxbinOP1 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(150, 100), size=(40, -1))  #input max
        self.input_minbinOP1 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(60, 100), size=(40, -1))  #input min
        self.input_maxbinOP2 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(150, 162), size=(40, -1))  #input max
        self.input_minbinOP2 = wx.TextCtrl(self.notebook_1_pane_3,value=' ', pos=(60, 162), size=(40, -1))  #input min


        self.input_max1p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 90), size=(60, -1))  #input max
        self.input_min1p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 120), size=(60, -1))  #input min
        self.input_max2p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 150), size=(60, -1))  #input max
        self.input_min2p2 = wx.TextCtrl(self.notebook_1_pane_2,value=' ', pos=(530, 180), size=(60, -1))  #input min

        self.input_x1 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 90),  size=(120, -1))  #input max
        self.input_x2 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 120), size=(120, -1))  #input min
        self.input_y1 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 150), size=(120, -1))  #input max
        self.input_y2 = wx.TextCtrl(self.notebook_1_pane_4, pos=(330, 180), size=(120, -1))  #input min

        #---------------------------------- buttons ------------------------------------

        self.btn_op1 = wx.Button(self.notebook_1_pane_1, label="Choose a file", pos=(340, 110), size=(150, -1)) #input file rg
        self.btn_op2 = wx.Button(self.notebook_1_pane_1, label="Choose a file", pos=(340, 140), size=(150, -1)) #input file rmsd
        self.btn_op1_op2 = wx.Button(self.notebook_1_pane_1, label="Choose a file", pos=(340, 170), size=(150, -1)) #input file rmsd

        btn_bw = wx.Button(self.notebook_1_pane_2, label='Summarize and Display', pos=(30, 40))
        #btn_bw = wx.Button(self.notebook_1_pane_3, label='Estimating bins', pos=(30, 40))
        #self.button1 = wx.Button(self.notebook_1_pane_2, label='Manual max and min', pos=(250, 40),size=(200, -1))

        btn_fes = wx.Button(self.notebook_1_pane_3, label='Compute FES', pos=(30, 260))
        btn_points = wx.Button(self.notebook_1_pane_4, label='Compute points', pos=(30, 150))
        #btn.SetFocus()


        #dirDlgBtn = wx.Button(self.notebook_1_pane_2, label="output folder", pos=(550, 80))
        self.dirDlgBtn1 = wx.Button(self.notebook_1_pane_3, label="Output folder", pos=(550, 80))
        #dirDlgBtn2 = wx.Button(self.notebook_1_pane_4, label="output folder", pos=(550, 80))
        btn_setexportfes = wx.Button(self.notebook_1_pane_3, label="Export FES file", pos=(550, 20))
        btn_settoplot = wx.Button(self.notebook_1_pane_3, label="Plot FES", pos=(550, 150))
        self.btn_exportfes = wx.Button(self.notebook_1_pane_3, label="Export", pos=(550, 115))
        self.btn_plotfes = wx.Button(self.notebook_1_pane_3, label="Plot", pos=(550, 230))

        #---------------------------------- Hide -----------------------------------------
        self.dirDlgBtn1.Hide()
        self.btn_exportfes.Hide()
        self.output_filename_fes.Hide()
        self.label_cfilename.Hide()
        self.label_cfolder.Hide()
        self.output_filename_fes.Hide()
        self.pt_fes.Hide()
        self.output_title_fes.Hide()
        self.btn_plotfes.Hide()

        #----
        self.sc.Hide()
        self.sc2.Hide()
        self.label_nbinsop1.Hide()
        self.label_nbinsop2.Hide()
        self.label_searchbins.Hide()
        self.label_op1bin.Hide()
        self.label_op2bin.Hide()
        self.input_maxbinOP1.Hide()
        self.input_minbinOP1.Hide()
        self.input_maxbinOP2.Hide()
        self.input_minbinOP2.Hide()




        #---------------------------------- events ------------------------------------

        btn_setexportfes.Bind(wx.EVT_BUTTON, self.button_setting_export_fes)
        btn_settoplot.Bind(wx.EVT_BUTTON, self.button_setting_to_plot)
        btn_fes.Bind(wx.EVT_BUTTON, self.OnCompute_fes)
        btn_bw.Bind(wx.EVT_BUTTON, self.OnCompute_bw)
        btn_points.Bind(wx.EVT_BUTTON, self.OnCompute_btn_points)
        self.btn_exportfes.Bind(wx.EVT_BUTTON, self.button_export_fes)
        self.btn_plotfes.Bind(wx.EVT_BUTTON, self.button_plotfes)

       # self.button1.Bind(wx.EVT_BUTTON, self.compute_mm_or_give_mm)

        self.btn_op1.Bind(wx.EVT_BUTTON, self.onOpenFile_op1) #self.onOpenFile_op1
        self.btn_op2.Bind(wx.EVT_BUTTON, self.onOpenFile_op2)
        self.btn_op1_op2.Bind(wx.EVT_BUTTON, self.onOpenFile_two_column)

        cb.Bind(wx.EVT_CHOICE, self.OnSelect)
        #cb2.Bind(wx.EVT_CHOICE, self.OnSelect2) #FES
        cb3.Bind(wx.EVT_CHOICE, self.OnSelect3)
        cb4.Bind(wx.EVT_CHOICE, self.OnSelect4)
        cb5.Bind(wx.EVT_CHOICE, self.OnSelect_cd5)
        cb6.Bind(wx.EVT_CHOICE, self.OnSelect_cd6)
        cb7.Bind(wx.EVT_CHOICE, self.OnSelect_cd7)
        cb8.Bind(wx.EVT_CHOICE, self.OnSelect_cd8)


        #dirDlgBtn.Bind(wx.EVT_BUTTON, self.onDir)
        self.dirDlgBtn1.Bind(wx.EVT_BUTTON, self.Dirtoexport)

        #---------------------------------- configuration of GUI ------------------------------------
                      #x ,  y
        self.SetSize((900, 600))
        self.SetTitle("S2FES")
        self.Centre()
        self.Show(True)






    def onDir(self, event):
        """
        Show the DirDialog and print( the user's choice to stdout
        """
        dlg = wx.DirDialog(self, "Choose a directory:",
                           style=wx.DD_DEFAULT_STYLE
                           #| wx.DD_DIR_MUST_EXIST
                           #| wx.DD_CHANGE_DIR
                           )
        if dlg.ShowModal() == wx.ID_OK:
            print("You chose %s" % dlg.GetPath())
        dlg.Destroy()

    def Dirtoexport(self, event):
        """
        Show the DirDialog and print( the user's choice to stdout
        """
        dlg = wx.DirDialog(self, "Choose a directory:",
                           style=wx.DD_DEFAULT_STYLE
                           #| wx.DD_DIR_MUST_EXIST
                           #| wx.DD_CHANGE_DIR
                           )
        if dlg.ShowModal() == wx.ID_OK:
            print("You chose %s" % dlg.GetPath())
            self.label_cfolder.SetLabel("%s" % dlg.GetPath())
            self.output_path_fesfile=dlg.GetPath()
        dlg.Destroy()


    def OnSelect(self, e):

        self.aut_manu_selec_btw = e.GetString()
        #self.st.SetLabel(i)

        if self.aut_manu_selec_btw =='Automatic max and min':
                #self.button1.SetLabel('Automatic max and min')
                self.max1p2.SetLabel("")
                self.min1p2.SetLabel("")
                self.max2p2.SetLabel("")
                self.min2p2.SetLabel("")
                self.input_max1p2.SetValue("")
                self.input_min1p2.SetValue("")
                self.input_max2p2.SetValue("")
                self.input_min2p2.SetValue("")
                self.input_max1p2.Hide()
                self.input_min1p2.Hide()
                self.input_max2p2.Hide()
                self.input_min2p2.Hide()

        else:
                #self.button1.SetLabel('Manual max and min')
                self.max1p2.SetLabel('OP1 max: ')
                self.min1p2.SetLabel('OP1 min: ')
                self.max2p2.SetLabel('OP2 max: ')
                self.min2p2.SetLabel('OP2 min: ')
                self.input_max1p2.Show()
                self.input_min1p2.Show()
                self.input_max2p2.Show()
                self.input_min2p2.Show()



    def OnSelect2(self, e):

        self.aut_manu_selec_fes = e.GetString()
        #self.st.SetLabel(i)

        if self.aut_manu_selec_fes =='Automatic max and min':
                #self.button1.SetLabel('Automatic max and min')
                self.max1p3.SetLabel("")
                self.min1p3.SetLabel("")
                self.max2p3.SetLabel("")
                self.min2p3.SetLabel("")
                self.input_max1p3.SetValue("")
                self.input_min1p3.SetValue("")
                self.input_max2p3.SetValue("")
                self.input_min2p3.SetValue("")
                self.input_max1p3.Hide()
                self.input_min1p3.Hide()
                self.input_max2p3.Hide()
                self.input_min2p3.Hide()



        else:
                #self.button1.SetLabel('Manual max and min')

                self.max1p3.SetLabel('OP1 max: ')
                self.min1p3.SetLabel('OP1 min: ')
                self.max2p3.SetLabel('OP2 max: ')
                self.min2p3.SetLabel('OP2 min: ')
                self.input_max1p3.Show()
                self.input_min1p3.Show()
                self.input_max2p3.Show()
                self.input_min2p3.Show()

    def OnSelect3(self, e):

        self.str_selec = e.GetString()
        #self.st.SetLabel(i)

        if self.str_selec =='Cell most populated':
                #self.button1.SetLabel('Automatic max and min')
                self.x1.SetLabel("")
                self.x2.SetLabel("")
                self.y1.SetLabel("")
                self.y2.SetLabel("")

                self.input_x1.SetValue("")
                self.input_x2.SetValue("")
                self.input_y1.SetValue("")
                self.input_y2.SetValue("")

                self.input_x1.Hide()
                self.input_x2.Hide()
                self.input_y1.Hide()
                self.input_y2.Hide()

        else:
                #self.button1.SetLabel('Manual max and min')

                self.x1.SetLabel('x1:')
                self.x2.SetLabel('x2:')
                self.y1.SetLabel('y1:')
                self.y2.SetLabel('y2:')
                self.input_x1.Show()
                self.input_x2.Show()
                self.input_y1.Show()
                self.input_y2.Show()


    def OnSelect4(self, e):

        self.str_selec1 = e.GetString()
        #self.st.SetLabel(i)

#        if self.str_selec =='mode':
#            self.str_selec1='mode'

#        if self.str_selec =='median':
#            self.str_selec1='median'

#        if self.str_selec =='mean':
#            self.str_selec1 ='median'


    def OnSelect_cd5(self, e):
        "Get the type file that was selected in cd5"
        self.type_file_selec_op1 = e.GetString()

    def OnSelect_cd6(self, e):
        "Get the type file that was selected in cd6"
        self.type_file_selec_op2 = e.GetString()

    def OnSelect_cd7(self, e):
        "Get the type file that was selected in cd6"
        self.type_file_selec_op1op2 = e.GetString()



    def OnSelect_cd8(self, e):

        self.aut_manu_selec_bins = e.GetString()
        # 'Automatic Prediction of Bins','Automatic Prediction of Bins From Specific Range','Specific Bins'

        if self.aut_manu_selec_bins =='Automatic Prediction of Bins':
             self.sc.Hide()
             self.sc2.Hide()
             self.label_nbinsop1.Hide()
             self.label_nbinsop2.Hide()
             self.label_searchbins.Show()
             self.label_op1bin.Hide()
             self.label_op2bin.Hide()
             self.input_maxbinOP1.Hide()
             self.input_minbinOP1.Hide()
             self.input_maxbinOP2.Hide()
             self.input_minbinOP2.Hide()
        elif self.aut_manu_selec_bins =='Automatic Prediction of Bins From Specific Range':
             self.sc.Hide()
             self.sc2.Hide()
             self.label_nbinsop1.Show()
             self.label_nbinsop2.Show()
             self.label_searchbins.Hide()
             self.label_op1bin.Hide()
             self.label_op2bin.Hide()
             self.input_maxbinOP1.Show()
             self.input_minbinOP1.Show()
             self.input_maxbinOP2.Show()
             self.input_minbinOP2.Show()
        else:
             self.sc.Show()
             self.sc2.Show()
             self.label_nbinsop1.Hide()
             self.label_nbinsop2.Hide()
             self.label_searchbins.Hide()
             self.label_op1bin.Show()
             self.label_op2bin.Show()
             self.input_maxbinOP1.Hide()
             self.input_minbinOP1.Hide()
             self.input_maxbinOP2.Hide()
             self.input_minbinOP2.Hide()



    def compute_mm_or_give_mm(self,e):
            if self.button1.GetLabel() =='Manual max and min':
                self.button1.SetLabel('Automatic max and min')
                self.max1p2.SetLabel("")
                self.min1p2.SetLabel("")
                self.max2p2.SetLabel("")
                self.min2p2.SetLabel("")
                self.input_max1p2.SetValue("")
                self.input_min1p2.SetValue("")
                self.input_max2p2.SetValue("")
                self.input_min2p2.SetValue("")
                self.input_max1p2.Hide()
                self.input_min1p2.Hide()
                self.input_max2p2.Hide()
                self.input_min2p2.Hide()
            else:
                self.button1.SetLabel('Manual max and min')
                self.max1p2.SetLabel('OP1 max: ')
                self.min1p2.SetLabel('OP1 min: ')
                self.max2p2.SetLabel('OP2 max: ')
                self.min2p2.SetLabel('OP2 min: ')
                self.input_max1p2.Show()
                self.input_min1p2.Show()
                self.input_max2p2.Show()
                self.input_min2p2.Show()


    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetTitle(("frame_1"))
        #self.SetBackgroundColour("Blue")
        #self.SetForegroundColour("Blue")
        self.SetFocus()
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        self.notebook_1.AddPage(self.notebook_1_pane_1, ("Input Data"))
        self.notebook_1.AddPage(self.notebook_1_pane_2, ("Input Data Display and Summary"))
        self.notebook_1.AddPage(self.notebook_1_pane_3, ("Compute FES"))
        self.notebook_1.AddPage(self.notebook_1_pane_4, ("Getting Structure"))
        sizer_1.Add(self.notebook_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        sizer_1.SetSizeHints(self)
        self.Layout()
        # end wxGlade



    def onOpenFile_op1(self, event):
        """
        Create and show the Open FileDialog #GetDirectory
        """
        tfs= self.type_file_selec_op1
        nm = self.op1_name_onecol.GetValue()

        self.path_op1=""
        self.data_op1=[]

        if tfs == "":
            self.ShowMessage1()

        elif  nm == "":
            self.ShowMessage2()

        elif tfs== 'gromacs rmsd' or tfs == 'gromacs rg' or  tfs =='gromacs PCA - proj1D' or tfs =='only one column':

            dlg = wx.FileDialog(
                self, message="Choose a "+tfs+" file (op1)",
                defaultFile="",
                wildcard="Python source (*.xvg; *.xvg)|*.xvg;*.xvg| All files (*.*)|*.*" ,
                # style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
                style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR
                )
            if dlg.ShowModal() == wx.ID_OK:
                paths = dlg.GetPath()
                self.path_op1=paths
                print( "You chose the following file(s):\n" + paths)

                if nm != "":
                    self.name_data_op1 = nm
                    print( "You chose the OP1 name: " + nm)

                if tfs== 'gromacs rmsd':
                    self.data_op1=self.get_rmsd(paths)['rmsd']
                    self.data_time=self.get_rmsd(paths)['time']
                elif tfs == 'gromacs rg':
                    self.data_op1=self.get_rg(paths)['rg']
                    self.data_time=self.get_rg(paths)['time']
                elif  tfs =='gromacs PCA - proj1D':
                    try:
                        self.data_op1=self.get_pca(paths,self.skip_header(paths))['pca']
                        self.data_time=self.get_pca(paths,self.skip_header(paths))['time']
                    except ValueError:
                        self.ShowMessage9()
                elif  tfs =='only one column':
                    self.data_op1=self.get_data_one_col(paths,"op1")
                    self.data_time=[]
                    print( self.data_op1)

                self.btn_op1.SetLabel(paths.split('/')[-1])

                print( "You chose the following type file:" + tfs)

            dlg.Destroy()


    def onOpenFile_op2(self, event):      #replace to order parameter 1
        """
        Create and show the Open FileDialog #GetDirectory
        """
        tfs= self.type_file_selec_op2
        nm = self.op2_name_onecol.GetValue()

        self.path_op2=""
        self.data_op2=[]

        if tfs == "":
            self.ShowMessage1()

        elif  nm == "":
            self.ShowMessage3()

        elif tfs== 'gromacs rmsd' or tfs == 'gromacs rg' or  tfs =='gromacs PCA - proj1D' or tfs =='only one column':

            dlg = wx.FileDialog(
                self, message="Choose a "+tfs+" file (op2)",
                defaultFile="",
                wildcard="Python source (*.xvg; *.xvg)|*.xvg;*.xvg| All files (*.*)|*.*" ,
                # style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
                style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR

                )
            if dlg.ShowModal() == wx.ID_OK:
                paths = dlg.GetPath()
                self.path_op2=paths
                print( "You chose the following file(s):\n" + paths)

                if nm != "":
                    self.name_data_op2 = nm
                    print( "You chose the OP2 name: " + nm)

                if tfs== 'gromacs rmsd':
                    self.data_op2=self.get_rmsd(paths)['rmsd']
                    self.data_time=self.get_rmsd(paths)['time']

                elif tfs == 'gromacs rg':
                    self.data_op2=self.get_rg(paths)['rg']
                    self.data_time=self.get_rg(paths)['time']
                elif  tfs =='gromacs PCA - proj1D':
                    try:
                        self.data_op2=self.get_pca(paths,self.skip_header(paths))['pca']
                        self.data_time=self.get_pca(paths,self.skip_header(paths))['time']
                    except ValueError:
                        self.ShowMessage9()
                elif  tfs =='only one column':
                    self.data_op2=self.get_data_one_col(paths,"op2")
                    self.data_time=[]

                self.btn_op2.SetLabel(paths.split('/')[-1])

                print( "You chose the following type file:" + tfs)

            dlg.Destroy()



    def onOpenFile_two_column(self, event):
        """
        Create and show the Open FileDialog #GetDirectory
        """

        #'gromacs PCA - proj2D', 'CSV file with two column'
        tfs= self.type_file_selec_op1op2
        self.data_op1=[]
        self.data_op2=[]

        if self.op1_name_twocol.GetValue()=="":
            self.ShowMessage4()

        elif self.op2_name_twocol.GetValue() =="" :
            self.ShowMessage5()

        elif tfs  =="":
            self.ShowMessage1()

        elif    self.op1_name_twocol.GetValue()!="" and self.op2_name_twocol.GetValue()!="":
            dlg = wx.FileDialog(
                self, message="Choose a file with two column",
                defaultFile="",
                wildcard="Python source (*.xvg; *.xvg)|*.xvg;*.xvg| All files (*.*)|*.*" ,
                                        style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR
                )
            if dlg.ShowModal() == wx.ID_OK:

                paths = dlg.GetPath()
                self.path_op1op2=paths
                print( "You chose the following file(s):"+ paths)

                self.name_data_op1 = self.op1_name_twocol.GetValue()
                self.name_data_op2 = self.op2_name_twocol.GetValue()
                print( "You chose the OP1 name: " + self.name_data_op1)
                print( "You chose the OP2 name: " + self.name_data_op2)

                if tfs== 'CSV file with two column' :
                    dataset_tmp=self.get_data_two_col(paths,"op1","op2")
                    self.data_op1=dataset_tmp["op1"]
                    self.data_op2=dataset_tmp["op2"]
                    self.data_time=[]

                elif   tfs== 'gromacs PCA - proj2D':

                    self.data_op1=self.get_pca_2d(paths,self.skip_header(paths))['pca1']
                    self.data_op2=self.get_pca_2d(paths,self.skip_header(paths))['pca2']
                    self.data_time=[]

                self.btn_op1_op2.SetLabel(paths.split('/')[-1])

            dlg.Destroy()


    def button_setting_export_fes(self, e):
        """
        Hide and show the setting to export the FES file
        """
#        if (self.fes_computed == True):
        if (self.export_fes_file==False):
                self.dirDlgBtn1.Show()
                self.btn_exportfes.Show()
                self.output_filename_fes.Show()
                self.label_cfilename.Show()
                self.label_cfolder.Show()
                self.export_fes_file=True
        elif (self.export_fes_file==True):
                self.dirDlgBtn1.Hide()
                self.btn_exportfes.Hide()
                self.output_filename_fes.Hide()
                self.label_cfilename.Hide()
                self.label_cfolder.Hide()
                self.export_fes_file=False
#        else:
#            self.ShowMessage7()



    def button_setting_to_plot(self, e):
        """
        Hide and show the setting to export the FES file
        """

       # if (self.fes_computed == True):

        if (self.plot_fes==False):

                self.btn_plotfes.Show()
                self.output_title_fes.Show()
                self.pt_fes.Show()
                self.plot_fes=True

        elif (self.plot_fes==True):

                self.btn_plotfes.Hide()
                self.output_title_fes.Hide()
                self.plot_fes=False
                self.pt_fes.Hide()

        #else:

         #   self.ShowMessage7()


#---------------------------- methods to show message ----------------------------------

    def ShowMessage1(self):
        wx.MessageBox('Select the type file!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage2(self):
        wx.MessageBox('Give a name to OP1!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage3(self):
        wx.MessageBox('Give a name to OP2!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage4(self):
        wx.MessageBox('Give a name to OP1!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage5(self):
        wx.MessageBox('Give a name to OP2!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage6(self): #remove. It is not beenig used.
        wx.MessageBox('Select manual or automatic max and min values!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage7(self):
        wx.MessageBox('Please, compute FES before!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage8(self):
        wx.MessageBox('Please, Select manual or automatic max and min values!', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage9(self):
        wx.MessageBox('Please, Check your input file. There may be some character in file end. Please remove it! ', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    def ShowMessage10(self,binx_max,biny_max):
        wx.MessageBox('The user should search the binwidth that is large than the sampling resolution of your data.' +
        ' For OP1 Bins the maximum value is ' + str(binx_max) + ' and for OP2 Bins the maximum value is ' + str(biny_max)+
        '. If you use this maximum values, it could require a high time of computing.'
        , 'Info',
            wx.OK | wx.ICON_INFORMATION)




#---------------------------- methods to open input files ----------------------------------
# Create new methods to allow the user upload RMSD, RG, PC1, PC2, etc ...

    def skip_header(self,infilename):     #--> IMP - use this for all method beyond get_pca
        """return the number of the lines of a file header """

        f=open(infilename, 'r')
        n=0
        l=f.readline()
        while ("#" in l or "@" in l):
            n=n+1
            l=f.readline()
        f.close()

        return n


    def get_pca(self,infilename,nlines):
        """Get the pca"""
        data_pca = genfromtxt(infilename,skip_header=nlines,
                              dtype={'names':['time', 'pca'],
                              'formats':[int,float]})
        return data_pca


    def get_pca_2d(self,infilename,nlines):
        """Get the pca"""
        data_pca = genfromtxt(infilename,skip_header=nlines,
                              dtype={'names':['pca1', 'pca2'],
                              'formats':[float,float]})
        return data_pca


    def get_rg(self,infilename):
        """Get the rg"""
        data_rg = genfromtxt(infilename,skip_header=22,
                        dtype={'names':['time', 'rg','rgx','rgy','rgz'],
                       'formats':[int,float,float,float,float]})
        return data_rg

    def get_rmsd(self,infilename):
        """Get the rmsd"""
        data_rmsd = genfromtxt(infilename,skip_header=13,
                        dtype={'names':['time', 'rmsd'],
                       'formats':[int,float]})
        return data_rmsd

    def get_data_one_col(self,infilename,name):
        """Get the data from file"""

        #data = genfromtxt(infilename, dtype={'names':[str(name)], 'formats':[float]})
        #problema aqui, -> [(1.82477,) (1.82132,) (1.82001,) ..., (1.65042,) (1.64056,) (1.64343,)]

        data = genfromtxt(infilename)

        return data

    def get_data_two_col(self,infilename,name1,name2):  #update it  to receive  onlt CSV file with 2 columns.
        """Get the data from file."""
        data = genfromtxt(infilename,
                        dtype={'names':[str(name1),str(name2)],
                       'formats':[float,float]},delimiter=",")
        return data

#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



    def button_export_fes(self, e):
        """Button to export fes    """
        #self.setting=(minv1, maxv1, minv2, maxv2, H,DG,xedges,yedges ,extent)

        path=self.output_path_fesfile+"/"+self.output_filename_fes.GetValue()
        self.write_fes_file(path, self.setting[4], self.setting[5], self.setting[6], self.setting[7])


    def button_plotfes(self, e):
        """Button to plot fes    """
        # Use this to create a new plot
        #http://stackoverflow.com/questions/6508769/matplotlib-scatter-hist-with-stepfilled-histtype-in-histogram

        path=self.output_path_fesfile+"/"+self.output_filename_fes.GetValue()
        #self.plot_fes_Contour(plt.cm.Spectral,path, self.setting[0],self.setting[1],self.setting[2],self.setting[3], 10)
        self.plot_fes_hist2d(self.setting[8],self.setting[5])




    def OnCompute_fes(self, e):
        """Performs the FES calculation with bins for OP1 and OP2 from:
            - Automatic Prediction of Bins
            - Automatic Prediction of Bins From Specific Range
            - Specific Bins
        """
        if self.aut_manu_selec_bins=='Automatic Prediction of Bins' or self.aut_manu_selec_bins=='Automatic Prediction of Bins From Specific Range' :
            x=self.data_op1            #save the data of order parameter 1
            y=self.data_op2
            H=Opthist2d(x,y)
            #import ipdb; ipdb.set_trace()

            dx=H.sample_resolution(H.data_x)
            dy=H.sample_resolution(H.data_y)

            Binx_max=H.large_binwidth(H.x_max, H.x_min,dx)
            Biny_max=H.large_binwidth(H.y_max, H.y_min,dy)
            print("large_binwidth of X and Y: ",Binx_max,Biny_max)

            if self.aut_manu_selec_bins=='Automatic Prediction of Bins From Specific Range' :
                if ( dx > (H.x_max - H.x_min)/float(self.input_maxbinOP1.GetValue()) and dy > (H.y_max - H.y_min)/float(self.input_maxbinOP2.GetValue()) ):
                    self.ShowMessage10(Binx_max,Biny_max)
                bounds = [(int(self.input_minbinOP1.GetValue()),int(self.input_maxbinOP1.GetValue())),
                          (int(self.input_minbinOP2.GetValue()),int(self.input_maxbinOP2.GetValue()))]
                #'Automatic Prediction of Bins From Specific Range'
                print('\nAutomatic Prediction of Bins From Specific Range')
                bins=Opthist2d(x,y).compute_cost_function_h2d_with_DA(bounds)
            else:
                #'Automatic Prediction of Bins',
                #The software will search the best pairs of Bins between [1,1000] for op1 and op2.
                print('\nAutomatic Prediction of Bins')
                bounds = [(1, 1001), (1, 1001)]
                bins=Opthist2d(x,y).compute_cost_function_h2d_with_DA(bounds)

            bin1 = bins[0]
            bin2 = bins[1]

            self.str_bestbin = ""
            self.str_bestbin = "The Bins pairs which were found and used to optimize the FES were\nOP1 Bins: "+str(bin1)+"\t\t\tOP2 Bins: "+str(bin2)+"\n\n\n"

        else:
            #self.max1p3.Show()
            bin1 = self.sc.GetValue()
            bin2 = self.sc2.GetValue()

            self.str_bestbin = "The bins used  OP1: "+str(bin1)+" OP2: "+str(bin2)+"\n\n\n"

        temp = self.sc3.GetValue() #get the value of temperature from GUI

        self.setting=self.fes( bin1, bin2 ,temp , None, None ,None,None)


    def fes(self,_bin1,_bin2 , _temp , data1_min=None,data1_max=None,data2_min=None,data2_max=None):
        """Create the FES and produces a output that has op1, op2 and Dg values"""

        print( "\n\nComputting the FES\n\n")
        self.fes_computed = True #I think it could be removed.

        if (data1_min==None or data1_max==None or data2_min==None or data2_max==None):
        #get the min and max ->  IMP - refactor -IMP
            data1_min = self.data_op1.min()
            data1_max = self.data_op1.max()
            data2_min = self.data_op2.min()
            data2_max = self.data_op2.max()
            minv1 = float(data1_min)
            maxv1 = float(data1_max)
            minv2 = float(data2_min)
            maxv2 = float(data2_max)
        else:
            minv1 = float(data1_min)
            maxv1 = float(data1_max)
            minv2 = float(data2_min)
            maxv2 = float(data2_max)

        extent = [minv1, maxv1, minv2, maxv2 ]

        #save a file with two columns: RG and RMSD
        #np.savetxt('rg_rmsd.out', np.array([data_rg['rg'],data_rmsd['rmsd']] ).transpose(), fmt='%f %f', delimiter=',')

        #bins
        bin1 = int(_bin1)
        bin2 = int(_bin2)

        V = np.zeros((bin1,bin2))
        DG = np.zeros((bin1,bin2))

        I1 = maxv1 - minv1 #remove
        I2 = maxv2 - minv2

        kB = 3.2976268E-24
        An = 6.02214179E23
        T = float(_temp) #temperature


        #old code to do the count
        x=self.data_op1
        y=self.data_op2
        r=[[minv1, maxv1], [minv2, maxv2]]


        #New code to do the count. It use the bins width that were got by rules or not
        #OBS.: A two dimensional histogram consists of a set of bins which count the number of events falling in a given area of the (x,y) plane.
        if(len(self.array_bins_edges)==4):
            if(bin1==(len(self.array_bins_edges[0])-1)  and bin2 ==(len(self.array_bins_edges[2])-1) ):
                print( 1,"use the bins width got by rules")
                H, xedges, yedges = np.histogram2d(x, y ,bins=[self.array_bins_edges[0],self.array_bins_edges[2]] ,range=r, normed=False)#bins=(bin1, bin2))
            elif (bin1==(len(self.array_bins_edges[1])-1)  and bin2 ==(len(self.array_bins_edges[3])-1) ):
                print( 2,"use the bins width got by rules")
                H, xedges, yedges = np.histogram2d(x, y, bins=[self.array_bins_edges[1],self.array_bins_edges[3]] , range=r, normed=False )#bins=(bin1, bin2))
            else:     #don't use the bins width got by rules
                print( 3,"don't use the bins width got by rules")
                H, xedges, yedges = np.histogram2d(x, y ,bins=(bin1, bin2),range=r, normed=False)
        else:         #don't use the bins width gotting by rules

            H, xedges, yedges = np.histogram2d(x, y ,bins=(bin1, bin2), range=r)
            print( 4,"don't use the bins width got by rules" )#,H, xedges, yedges)

        # Finding maximum number of points inside of a cell.  #I can use this ->  np.where(x == np.max(x))
        P = list()
        for x in range(H.shape[0]):
            for y in range(H.shape[1]):
                P.append(H[x][y])
                print(H[x][y],x,y)
        Pmax = max(P) #value of maximum number of points inside of a cell
        print("Pmax: ",Pmax)

        LnPmax = math.log(Pmax)

        print( "\n\nbins that were used: ",H.shape)

        #Compute Gibbs free energy landscapes
        for x in range(H.shape[0]):
            for y in range(H.shape[1]):
    	        if H[x][y] == 0:
    		        DG[x][y] = 10
            		continue
            	else:
    		        DG[x][y] = -0.001*An*kB*T*(math.log(H[x][y])-LnPmax)

        #Always will have a number maximum of the point inside of one  or more cells.
        #The cell(s) with the maximum number of points will be the cell in minimum of energy.
        #So it is import verify how many cells have the number maximum of points.

        #test about cell(s) with the most number of point
        cells=np.where(H == np.max(H))
        cells=np.stack(cells,axis=1)
        #print( np.where(H == np.max(H)),H[np.where(H == np.max(H))],Pmax,np.where(DG == np.min(DG)),DG[np.where(DG == np.min(DG))]

        self.cells_energy_minimum=[]
        for cell in cells:
            self.cells_energy_minimum.append([xedges[cell[0]], xedges[cell[0]+1], yedges[cell[1]], yedges[cell[1]+1]])

        str_output=""
        ncells=1
        str_header= "Total of "+str(len(cells))+" with maximum number of points.\n"
        for cell in cells:
            count_c=0
            for i in range(len(self.data_op1)):
                if ( xedges[cell[0]]<= self.data_op1[i]   <=xedges[cell[0]+1]     and    yedges[cell[1]]<=   self.data_op2[i]  <=yedges[cell[1]+1]):
                    count_c=count_c+1
            str_output= "Cell_"+str(ncells)  +" has "+str(count_c)+" points and coordinates \n x1: "+ str(xedges[cell[0]]) +" x2: " +str(xedges[cell[0]+1]) + "\n y1: "+ str(yedges[cell[1]]) +" y2: " + str(yedges[cell[1]+1]) + "\n" + str_output
            ncells=ncells+1
        cell_information=str_header +str_output
        print(cell_information)

        #compute the structure more "stable" in the cell of the least free energy
        self.outputbox2.SetValue(self.str_bestbin+cell_information)

        #return  (outfilename,minv1, maxv1, minv2, maxv2, fontsize_tick)
        return  (minv1, maxv1, minv2, maxv2, H,DG,xedges,yedges ,extent)





    def OnCompute_bw(self, e):
        """Execute the method to estimating the bin width"""
        if self.aut_manu_selec_btw=="":
            self.ShowMessage8()
        else:
            self.input_data_display_and_summary()
            print( "\n\n\nEstimating the bin width for each dataset.\n\n")


    def OnCompute_btn_points(self, e):
        """compute points ... """

        #self.cells_energy_minimum  #=[xedges[ai[0]], xedges[ai[0]+1], yedges[ai[1]], yedges[ai[1]+1] ]

        data_op1 = self.data_op1 #self.get_rg(self.path_rg)
        data_op2 = self.data_op2 #self.get_rmsd(self.path_rmsd)

        breakloop=False

        for cell in self.cells_energy_minimum:
            input_data_x=[]
            input_data_y=[]
            input_data_time=[]
            count_c=0

            if self.str_selec =='Cell most populated':

                for i in range(len(data_op1)):
                    if ( cell[0] <= data_op1[i] <= cell[1] and
                         cell[2] <= data_op2[i] <= cell[3]):
                        count_c=count_c+1
                        input_data_x.append(data_op1[i])
                        input_data_y.append(data_op2[i])

                        if self.data_time!=[]: #update it
                            input_data_time.append(self.data_time[i])
            else:
                for i in range(len(data_op1)):
                    if ( float(self.input_x1.GetValue()) <= data_op1[i] <= float(self.input_x2.GetValue()) and
                         float(self.input_y1.GetValue()) <= data_op2[i] <= float(self.input_y2.GetValue())):
                        count_c=count_c+1
                        input_data_x.append(data_op1[i])
                        input_data_y.append(data_op2[i])

                        if self.data_time!=[]:
                            input_data_time.append(self.data_time[i])
                breakloop=True


            #print( self.input_x1.GetValue(),self.input_x2.GetValue(),self.input_y1.GetValue(),self.input_y2.GetValue()

            reference_point=self.compute_point(input_data_x,input_data_y,met=self.str_selec1)

            dist2points= self.compute_dist2points(reference_point[0],reference_point[1],input_data_x,input_data_y,input_data_time)
            print( dist2points)

            str_output="The nearest points from the point that was computed\n\ndist\t\t\t\t\tx\t\t\ty\t\t\t\ttime"
            for l in range(self.sc4.GetValue()):
    #            s="%\nf \t%f \t%f"% (dist2points[l][0],dist2points[l][1],dist2points[l][2])  #4.40958001e-06 	1.62743 	0.9995489 	295600.0
                if self.data_time!=[]:
                    s="\n" +str(dist2points[l][0] )+" \t"+str(dist2points[l][1])+" \t" +str(dist2points[l][2])+" \t" +str(dist2points[l][3])
                else:
                    s="\n" +str(dist2points[l][0] )+" \t"+str(dist2points[l][1])+" \t" +str(dist2points[l][2])
                str_output=str_output+ s
            self.outputbox3.SetValue("The region selected has "+str(count_c)+" points.\n" + str_output)

            #-------------------------------------plot

            matplotlib.rcParams['axes.unicode_minus'] = False
            fig, ax = plt.subplots()
            ax.plot(input_data_x,input_data_y, 'o')
            arrstyles = ['-', '->', '-[', '<-', '<->', 'fancy', 'simple','wedge']
            style=arrstyles[-2]
    #        plt.annotate(self.str_selec1+"  \n" , xytext=(reference_point[0]+0.00001,reference_point[1]+0.00001), xy=(reference_point[0], reference_point[1]),arrowprops=dict(arrowstyle=style,facecolor='red'));

            plt.annotate(str("Point created from "+self.str_selec1+ " of x and y"), xytext=(-20,20), xy=(reference_point[0], reference_point[1]),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
            ax.plot(reference_point[0], reference_point[1], 'D')

            ax.set_title('Points inside the region selected')
            #plt.draw()

            if breakloop==True:
                break
                print('break')

        plt.show()


    def compute_hist_bins(self, style, data, name,  ax=None,mmin=None,mmax=None):
        """
         Compute a histogram and use of more sophisticated algorithms for determining bins.
         Algorithms:
            'scott' - use Scott's rule to determine bins
            'freedman' - use the Freedman-diaconis rule to determine bins
        """
        if ax is not None:
            ax = plt.axes(ax)

        if self.aut_manu_selec_btw =='Manual max and min':
            #Compute the number of bins, bins edges
            # counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step', normed=False , range=(float(mmin), float(mmax)))  # IMP
            counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step' , range=(float(mmin), float(mmax)))  # IMP
            self.array_bins_edges.append(bins) #save the bins edges that were computed to be used to compute FES.
            ax.text(0.99, 0.95, '%s:\n%i bins' % (name, len(counts)), transform=ax.transAxes,ha='right', va='top', fontsize=12)

        else:
            #Compute the number of bins, bins edges
            # counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step', normed=False )  # IMP
            counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step' )  # IMP
            self.array_bins_edges.append(bins) #save the bins edges that were computed to be used to compute FES.
            ax.text(0.99, 0.95, '%s:\n%i bins' % (name, len(counts)), transform=ax.transAxes,ha='right', va='top', fontsize=12)

        ax.set_xlim(bins[0], bins[-1])

        return counts, bins #,ax


    def cost_function_hist1d_x(self,N):
        """Cost function for histogram 1D with data X - data_op1"""
        Nx=N[0]
        Nx=max(Nx, 1)
        Nx=int(Nx)
        Dx=(self.data_op1.max() - self.data_op1.min())/float(Nx)
        ki = histogram(self.data_op1, bins=Nx)
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cx = (2 * k - v) / ( (Dx)**2 )  #The cost Function
        #print(Cx,Dx,Nx,self.x_max,self.x_min)
        return Cx


    def cost_function_hist1d_y(self,N):
        """Cost function for histogram 1D with data Y - data_op2"""
        Ny=N[0]
        Ny=max(Ny, 1)
        Ny=int(Ny)
        Dy=(self.data_op2.max() - self.data_op2.min())/float(Ny)
        ki = histogram(self.data_op2, bins=Ny)
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cy = (2 * k - v) / ( (Dy)**2 )  #The cost Function
        return Cy

    def plot_dendrogram(self, model, **kwargs):
        # Create linkage matrix and then plot the dendrogram

        # create the counts of samples under each node
        counts = np.zeros(model.children_.shape[0])
        n_samples = len(model.labels_)
        for i, merge in enumerate(model.children_):
            current_count = 0
            for child_idx in merge:
                if child_idx < n_samples:
                    current_count += 1  # leaf node
                else:
                    current_count += counts[child_idx - n_samples]
            counts[i] = current_count

        linkage_matrix = np.column_stack([model.children_, model.distances_,
                                          counts]).astype(float)

        # Plot the corresponding dendrogram
        dendrogram(linkage_matrix, **kwargs)

    def input_data_display_and_summary(self):
        """
        Dendrogram
        Cluster with k-means
        Plot six distributions with the bins estimated by Shimazaki and Shinomoto, Scott's and Freedman-diaconis rules.
        """

        op1=self.data_op1
        op2=self.data_op2

        data1_min = op1.min()
        data1_max = op1.max()
        data2_min = op2.min()
        data2_max = op2.max()





        #https://medium.com/analytics-vidhya/how-to-determine-the-optimal-k-for-k-means-708505d204eb
        #https://towardsdatascience.com/k-means-clustering-with-scikit-learn-6b47a369a83c
        #https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
        X = np.stack((op1,op2), axis=1)
        #fig = figure()
        # plt.scatter(
        #    X[:, 0], X[:, 1],
        #    c='white', marker='o',
        #    edgecolor='black', s=50
        # )
        # plt.show()
        km = KMeans(
            n_clusters=3, init='random',
            n_init=10, max_iter=300,
            tol=1e-04, random_state=0
        )
        y_km = km.fit_predict(X)

        fig = figure()
        # plot the 3 clusters
        plt.scatter(
            X[y_km == 0, 0], X[y_km == 0, 1],
            s=5, c='lightgreen',
            marker='o', edgecolor='black',
            label='cluster 1'
        )
        plt.scatter(
            X[y_km == 1, 0], X[y_km == 1, 1],
            s=50, c='orange',
            marker='s', edgecolor='black',
            label='cluster 2'
        )
        plt.scatter(
            X[y_km == 2, 0], X[y_km == 2, 1],
            s=50, c='lightblue',
            marker='v', edgecolor='black',
            label='cluster 3'
        )
        # plot the centroids
        plt.scatter(
            km.cluster_centers_[:, 0], km.cluster_centers_[:, 1],
            s=150, marker='*',
            c='red', edgecolor='black',
            label='Centroid x: '+str(km.cluster_centers_[:, 0])+' / y: '+str(km.cluster_centers_[:, 1])
        )
        plt.xlabel(self.name_data_op1)
        plt.ylabel(self.name_data_op2)
        plt.title('K-Means for '+self.name_data_op1+" and "+self.name_data_op2)
        plt.legend(scatterpoints=1)
        plt.grid()
        #plt.show()


        #https://scikit-learn.org/stable/auto_examples/cluster/plot_agglomerative_dendrogram.html#sphx-glr-auto-examples-cluster-plot-agglomerative-dendrogram-py
        #Plot Dendrogram
        X = np.stack((op1,op2), axis=1)
        # setting distance_threshold=0 ensures we compute the full tree.
        model = AgglomerativeClustering(distance_threshold=0.5, n_clusters=None)
        model = model.fit(X)
        fig = figure()
        plt.title('Hierarchical Clustering Dendrogram for '+self.name_data_op1+" and "+self.name_data_op2)
        # plot the top three levels of the dendrogram
        self.plot_dendrogram(model, truncate_mode='level', p=3)
        plt.xlabel("Number of points in node (or index of point if no parenthesis).")
        # plt.show()



        #Plot six distributions with the bins estimated by Shimazaki and Shinomoto, Scott's and Freedman-diaconis rules.
        ranges_1d=(slice(1, 20000,1),)
        print("brute force runnig in x")
        start_time = time.time()
        outputX=optimize.brute(self.cost_function_hist1d_x,ranges_1d, disp=True, finish=None)
        time_DE="%s" % (time.time() - start_time)
        print(outputX)
        print("\nTotal time: "+str(time_DE))
        print("finished")
        print("brute force runnig in y")
        start_time = time.time()
        outputY=optimize.brute(self.cost_function_hist1d_y,ranges_1d, disp=True, finish=None)
        time_DE="%s" % (time.time() - start_time)
        print(outputY)
        print("\nTotal time: "+str(time_DE))
        print("finished")

        fig, axs = plt.subplots(3, 2, sharey=True, tight_layout=True)
        print(axs)
        axs[0][0].hist(op1, bins=int(outputX),color='#00cc66', rwidth=0.9)
        #axs[0][0].set_xlabel('time(sec)')
        axs[0][0].set_ylabel('Count')
        axs[0][0].set_title('Distribution '+self.name_data_op1)
        axs[0][0].text(0.99, 0.95, 'Bins from Shimazaki and Shinomoto: %i\nMean: %f \nMax value: %f \nMin value: %f ' %
        (int(outputX),mean(op1),max(op1),min(op1)), transform=axs[0][0].transAxes,ha='right', va='top', fontsize=11)

        axs[0][1].hist(op2, bins=int(outputY),color='#0066cc', rwidth=0.9)
        #axs[0][1].set_xlabel('time(sec)')
        axs[0][1].set_title('Distribution '+self.name_data_op2)
        axs[0][1].text(0.99, 0.95, 'Bins from Shimazaki and Shinomoto: %i\nMean: %f \nMax value: %f \nMin value: %f ' %
        (int(outputY),mean(op2),max(op2),min(op2)), transform=axs[0][1].transAxes,ha='right', va='top', fontsize=11)

        counts, bins, patches = hist(op1, bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
        axs[1][0].hist(op1, bins=len(counts),color='#00cc66', rwidth=0.9)
        #axs[1][0].set_xlabel('time(sec)')
        axs[1][0].set_ylabel('Count')
        #axs[1][0].set_title('Total time (sec) of every executions of DA with 8000 (RUN3)')
        axs[1][0].text(0.99, 0.95, 'Bins from Freedman-Diaconis: %i\nMean: %f \nMax value: %f \nMin value: %f ' %
        (len(counts),mean(op1),max(op1),min(op1)), transform=axs[1][0].transAxes,ha='right', va='top', fontsize=11)

        counts, bins, patches = hist(op2, bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
        axs[1][1].hist(op2, bins=len(counts),color='#0066cc', rwidth=0.9)
        #axs[1][1].set_xlabel('time(sec)')
        #axs[1][1].set_title('Total time (sec) of every executions of DE with 5000 (RUN3)')
        axs[1][1].text(0.99, 0.95, 'Bins from Freedman-Diaconis: %i\nMean: %f \nMax value: %f \nMin value: %f' %
        (len(counts),mean(op2),max(op2),min(op2)), transform=axs[1][1].transAxes,ha='right', va='top', fontsize=11)

        counts, bins, patches = hist(op1, bins='scott', ax=None, histtype='stepfilled', alpha=0.2, density=True)
        axs[2][0].hist(op1, bins=len(counts),color='#00cc66', rwidth=0.9)
        axs[2][0].set_xlabel(self.name_data_op1)
        axs[2][0].set_ylabel('Count')
        #axs[2][0].set_title('Total time (sec) of every executions of DA with 8000 (RUN2)')
        axs[2][0].text(0.99, 0.95, 'Bins from Scott`s rule: %i\nMean: %f \nMax value: %f \nMin value: %f ' %
         (len(counts),mean(op1),max(op1),min(op1)), transform=axs[2][0].transAxes,ha='right', va='top', fontsize=11)

        counts, bins, patches = hist(op2, bins='scott', ax=None, histtype='stepfilled', alpha=0.2, density=True)
        axs[2][1].hist(op2, bins=len(counts),color='#0066cc', rwidth=0.9)
        axs[2][1].set_xlabel(self.name_data_op2)
        #axs[2][1].set_title('Total time (sec) of every executions of DE with 4000 (RUN2)')
        axs[2][1].text(0.99, 0.95, 'Bins from Scott`s rule: %i\nMean: %f \nMax value: %f\nMin value: %f ' %
         (len(counts),mean(op2),max(op2),min(op2)), transform=axs[2][1].transAxes,ha='right', va='top', fontsize=11)

        #plt.show()


        #Plot four distributions with the bins estimated by Scott's and Freedman-diaconis rules.
        fig = plt.figure(figsize=(10, 5))
        fig.subplots_adjust(hspace=0.1, left=0.1, right=0.95, wspace=0.1)
        ax = [fig.add_subplot(2, 2, i + 1) for i in range(4)]

        self.array_bins_edges=[]

        array_info_hist=[]

        # first column: data1
        array_info_hist.append(  #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('scott', op1, 'Scott\'s Rule', ax=ax[0],mmin=self.input_min1p2.GetValue(), mmax=self.input_max1p2.GetValue()), #first elem. of array
                        self.name_data_op1,'Scott\'s Rule',data1_min, data1_max #others elements
                ])

        array_info_hist.append( #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('freedman', op1, 'Freedman-Diaconis', ax=ax[2],mmin=self.input_min1p2.GetValue(), mmax=self.input_max1p2.GetValue()), #first elem. of array
                        self.name_data_op1,  'Freedman-Diaconis',data1_min, data1_max #others elements
                ])

        ax[0].set_title('Distribution '+self.name_data_op1)
        ax[0].set_ylabel('Count')
        ax[2].set_ylabel('Count')
        ax[2].set_xlabel(self.name_data_op1)

        # second column: data2
        array_info_hist.append( #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('scott', op2, 'Scott\'s Rule', ax=ax[1], mmin=self.input_min2p2.GetValue(),mmax=self.input_max2p2.GetValue()), #first elem. of array
                        self.name_data_op2,'Scott\'s Rule',data2_min, data2_max #others elements
                ])

        array_info_hist.append( #get the information about each distribution to send outputbox1
                [
                self.compute_hist_bins('freedman', op2, 'Freedman-Diaconis', ax=ax[3], mmin=self.input_min2p2.GetValue(),mmax=self.input_max2p2.GetValue()), #first elem. of array
                       self.name_data_op2,'Freedman-Diaconis',data2_min, data2_max #others elements
                ])

        ax[1].set_title('Distribution '+self.name_data_op2)
        ax[3].set_xlabel(self.name_data_op2)

        plt.show()



        self.str_output_hist_inf=""  #send the information about the distribution to outputbox1
        for i in array_info_hist:
            self.str_output_hist_inf = self.str_output_hist_inf + ('data: %s\n%s: %i bins \nHistogram\nmin: %f\nmax: %f\nData\nmin: %f\nmax: %f\n\n' %
                                  (i[1],i[2], len(i[0][0]), (i[0][1][0]) ,(i[0][1][-1]) ,float(i[3]),float(i[4]) ))

        for i in array_info_hist:
            #put this information in log file
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\n" + i[2] +" rule to determine bins to " + i[1] +" data."
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\nNumber of bins: "+ str(len(i[0][0]))+ " and number of bin edges: "+  str(len(i[0][1]))
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\nCounts in each bin: "
            for j in i[0][0]:
                self.str_output_hist_inf= self.str_output_hist_inf+ "\n"+str(j)
            self.str_output_hist_inf= self.str_output_hist_inf+ "\n\nBin edges: "
            for j in i[0][1]:
                self.str_output_hist_inf= self.str_output_hist_inf+ "\n"+str(j)

        self.outputbox1.SetValue(self.str_output_hist_inf)







    def write_fes_file(self,outfilename,H,DG,xedges,yedges):
        """
        Produces the output file with three columns rg, rmsd, DG.
        I was using the media of the each edges correspondet to each DG.
        """
        ofile = open(outfilename,'w')    # open file for writing
        for x in range(H.shape[0]):
        	for y in range(H.shape[1]):
        		#ofile.write(str((xedges[x]+xedges[x+1])/2.) + "\t" + str((yedges[y]+yedges[y+1])/2.) + "\t" + str(DG[x][y])+"\n")
        		ofile.write(str(xedges[x]) + "\t" + str(yedges[y]) + "\t" + str(DG[x][y])+"\n")
        	ofile.write("\n")
        ofile.close()




    def compute_point(self,datax,datay,met="mean"):
        """"Compute the reference point which could be mean,
             median, mode and centroid from data in x and y"""

        if met=="mean":
            print( "mean")
            x=np.mean(datax)
            y=np.mean(datay)

        if met=="centroid":
            print( "centroid")
            x=np.mean(datax)
            y=np.mean(datay)

        if met=="median":
            print( "median")
            x=np.median(datax)
            y=np.median(datay)

        if met=="mode":
            print( "mode")
            x=stats.mode(datax)
            y=stats.mode(datay)
            x=x[0][0]
            y=y[0][0]

        return (x,y)


    def compute_dist2points(self,pmx,pmy,datax,datay,datatime):
        "compute the distance betwen mean point and all points"
        distpoint=[]
        for i in range(len(datax)):
            if self.data_time!=[]:
                p = ( float(sqrt((pmx-datax[i])**2) + ((pmy-datay[i])**2)) , float(datax[i]) ,float(datay[i]),float(datatime[i]) )
            else:
                p = ( float(sqrt((pmx-datax[i])**2) + ((pmy-datay[i])**2)) , float(datax[i]) ,float(datay[i]) )
            distpoint.append(p)
            print( p)
        print( "\n\nsort the points with the least distance from mean point.\n\n")

        if self.data_time!=[]:
            distAB = np.array( distpoint , dtype=[('dist', float),('x', float), ('y', float),('time', float)])
        else:
            distAB = np.array( distpoint , dtype=[('dist', float),('x', float), ('y', float)])

        distAB.sort(order='dist')   #sort the points with the least distance from mean point
        return distAB



    def plot_fes_hist2d(self,extent,DG):


        #---------------------------- criar um funcao fora remove------------------------------------------------------
        #create a method to produce this plots
        Hmasked = np.ma.masked_where(DG==10,DG)

        #for i in range(50):

        plt.imshow( Hmasked.T,extent=extent,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
        plt.colorbar().set_label(r'$\Delta G(kcal/mol)$')
        plt.xlabel(self.name_data_op1)
        plt.ylabel(self.name_data_op2)
        plt.title(str(self.output_title_fes.GetValue())) #self.output_title.GetLabel()

#        plt.set_label('$\Delta G(Kcal/mol)$',fontsize=17)
#        plt.colorbar()
        plt.show()

        #    plt.savefig("filename"+str(i), dpi=100)
        #    plt.clf()

        #for i in range(50):

        #plt.imshow(Hmasked.T,extent=extent,interpolation='nearest',origin='lower',aspect='auto')
        #plt.colorbar()
        #plt.show()

        #    plt.savefig("filename"+str(i+50), dpi=100)
        #    plt.clf()

        #for i in range(50):

        #plt.imshow(Hmasked.T,extent=extent,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.gnuplot)
        #plt.colorbar()
        #plt.show()

        #    plt.savefig("filename"+str(i+100), dpi=100)
        #    plt.clf()

        #os.system('mencoder mf://*.png -mf type=png:w=800:h=600:fps=25 -ovc lavc -lavcopts vcodec=mpeg4 -oac copy -o output.avi')


        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


    def plot_fes_Contour(self,colorscale,outfilename,data1_min,data1_max,data2_min,data2_max, fontsize_tick):
        """Produces contour plot"""

        data_fes = genfromtxt(outfilename, #skip_header=22,
                        dtype={'names':['rg','rmsd','Dg'],
                       'formats':[float,float,float]})
        # make up data.
        #npts = int(raw_input('enter # of random points to plot:'))
        npts = 200 #  np.shape(data_fes['rg'])[0]

        x = data_fes['rg']
        y = data_fes['rmsd']
        z = data_fes['Dg']



        # define grid.
        xi = np.linspace(data1_min,data1_max,npts)
        yi = np.linspace(data2_min,data2_max,npts)
        # grid the data.
        #zi = griddata(x,y,z,xi,yi,interp='linear') #problem with interp='linear'
        zi = griddata(x,y,z,xi,yi)
        # contour the gridded data, plotting dots at the nonuniform data points.
        CS = plt.contour(xi,yi,zi,10,linewidths=0.001,colors='k')

        # rainbow |

        CS = plt.contourf(xi,yi,zi,100,cmap=colorscale,
                      vmax=abs(zi).max(), vmin=-abs(zi).max())


        cb=plt.colorbar(orientation="vertical") #WHY?????????????????????????????
        cb.set_label('$\Delta G(kcal/mol)$',fontsize=17)
        plt.xlabel(self.name_data_op1)
        plt.ylabel(self.name_data_op2)
        # plot data points.
        #plt.scatter(x,y,marker='o',c='b',s=5,zorder=10)
        plt.xlim(data1_min,data1_max)
        plt.ylim(data2_min,data2_max)

        override = {
       'fontsize'            : 50,
       'verticalalignment'   : 'center',
       'horizontalalignment' : 'right'}

        plt.yticks( fontsize=fontsize_tick )
        plt.xticks( fontsize=fontsize_tick )
        #plt.ylabel("RMSD",override)
        #plt.xlabel("RMSD",override)

        plt.title(str(self.output_title_fes.GetValue()))
        plt.show()












def main():

    ex = wx.App()
    Big_house_tools(None)
    ex.MainLoop()



if __name__ == '__main__':
    main()
