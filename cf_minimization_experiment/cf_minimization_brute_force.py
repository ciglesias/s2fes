from numpy import array, arange, argmin, sum, mean, var, size, zeros,	where, histogram
from numpy.random import normal
from matplotlib.pyplot import figure, plot, hist, bar, xlabel, ylabel,title, show, savefig
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize
from past.builtins import xrange
import pandas as pd
import time


#https://www2.hawaii.edu/~jonghyun/classes/S18/CEE696/files/14_global_optimization.pdf
# https://perso.crans.org/besson/publis/notebooks/Simulated_annealing_in_Python.html
# https://pablormier.github.io/2017/09/05/a-tutorial-on-differential-evolution-with-python/
# https://matplotlib.org/3.2.1/gallery/index.html
# https://plotly.com/python/2D-Histogram/#2d-histogram-overlaid-with-a-scatter-chart
# https://stackoverflow.com/questions/27156381/python-creating-a-2d-histogram-from-a-numpy-matrix


class Opthist2d():


    def __init__(self,data_x,data_y):

        self.data_x=data_x
        self.data_y=data_y
        self.x_max = max(data_x)
        self.x_min = min(data_x)
        self.y_max = max(data_y)
        self.y_min = min(data_y)
        self.Nx_MIN = 1   #Minimum number of bins in x (integer)
        self.Nx_MAX = 100  #Maximum number of bins in x (integer)
        self.Ny_MIN = 1 #Minimum number of bins in y (integer)
        self.Ny_MAX = 100  #Maximum number of bins in y (integer)
        self.Dxy=[]
        self.Cxy=[]
        self.Cxy_Dxy_plot =[] #save data to plot in scatterplot x,y,z


    def sample_resolution(self,data):
        """Compute and return the sample resolution of some data"""
        buf = abs(np.diff(np.sort(data)))
        dx = np.min(buf[buf != 0])
        return dx

    def binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / float(samp_resol)
        return N_max

    def large_binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / samp_resol/ 2
        return int(N_max)

    def arrayofbins(self,N_MIN,N_MAX):
        """Return a array of bins """
        return arange(N_MIN, N_MAX) # #of Bins

    def arrayofbinwidths(self,datamax,datamin, N):
        """Return a array of bins """
        return (datamax - datamin) / N  #Bin size vector

    def matrixofbinwidths(self,Dx,Dy):
        """Set the matrix of binwidths and save in self.Dxy"""
        for i in Dx:    #Bin size vector
            a=[]
            for j in Dy:    #Bin size vector
                a.append((i,j))
            self.Dxy.append(a)
        self.Dxy=array(self.Dxy, dtype=[('x', float),('y', float)]) #matrix of bin size vector
        return self.Dxy

    def cost_function(self,Nx,Ny):
        """ Compute the cost function pairs of binwidths """
        self.Cxy=zeros(np.shape(self.Dxy))
        #Computation of the cost function to x and y
        for i in xrange(size(Nx)):
            for j in xrange(size(Ny)):
                ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx[i],Ny[j]))
                ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
                k = mean(ki) #Mean of event count
                v = var(ki)  #Variance of event count
                self.Cxy[i,j] = (2 * k - v) / ( (self.Dxy[i,j][0]*self.Dxy[i,j][1])**2 )  #The cost Function
                            #(Cxy      , Dx          ,  Dy)
                self.Cxy_Dxy_plot.append((self.Cxy[i,j] , self.Dxy[i,j][0] , self.Dxy[i,j][1]))#Save result of cost function to scatterplot
        self.Cxy_Dxy_plot = array( self.Cxy_Dxy_plot , dtype=[('Cxy', float),('Dx', float), ('Dy', float)])  #Save result of cost function to scatterplot
        print( "ok")

    def cftest(self,N):
        """ Compute the cost function using brute force """
        Nx,Ny=N

        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        #self.v=self.v+1
       #print( self.v
        return Cxy

    def cf(self,N):
        """ Compute the cost function using brute force """
        Nx,Ny=N
        Nx=max(Nx, 1)
        Ny=max(Ny, 1)
        Nx=int(Nx)
        Ny=int(Ny)
        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        print(N)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        # print (k,v, (2 * k - v))
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        #import ipdb; ipdb.set_trace()
        #self.v=self.v+1
        #print( self.v
        # print(N,Nx,Ny,Cxy)
        return Cxy




    def idx_min_cost_function(self):
        """return the index of combination of i and j that produces the minimum cost function"""
        idx_min_Cxy=np.where(self.Cxy == np.min(self.Cxy)) #get the index of the min Cxy
        return (idx_min_Cxy[0][0],idx_min_Cxy[1][0])




def main():
    #COMPUTE THE COST FUNCTION
    #-------------------------
    from numpy import genfromtxt
    data_rg = genfromtxt("../test_codes_hopt/bid-rg-idx-3.xvg"
                            ,skip_header=22,
                            dtype={'names':['time', 'rg','rgx','rgy','rgz'],
                           'formats':[int,float,float,float,float]})
    data_rmsd = genfromtxt("../test_codes_hopt/bid-rmsd-idx-46_3.xvg",skip_header=13,
                            dtype={'names':['time', 'rmsd'],
                           'formats':[int,float]})
    y=data_rmsd['rmsd']
    x=data_rg['rg']

    H=Opthist2d(x,y)

    dx=H.sample_resolution(H.data_x)
    dy=H.sample_resolution(H.data_y)
    print("sample_resolution of X and Y: ",dx,dy)

    Nx_max=H.large_binwidth(H.x_max, H.x_min,dx)
    Ny_max=H.large_binwidth(H.y_max, H.y_min,dy)
    print("large_binwidth of X and Y: ",Nx_max,Ny_max)


    ranges=(slice(1, 1000,1),) * 2

    print("brute force runnig with: ",ranges)
    file = open('optimize_brute_cf_bins_1000x1000.csv','w')
    file.write("Results\n")
    start_time = time.time()
    output=optimize.brute(H.cf,ranges, disp=True, finish=None)
    time_DE="%s" % (time.time() - start_time)
    file.write("\nbin_x: "+str(output[0]))
    file.write("\nbin_y: "+str(output[1]))
    file.write("\nTotal time: "+str(time_DE))
    print("finished")


if __name__ == '__main__':
    main()
