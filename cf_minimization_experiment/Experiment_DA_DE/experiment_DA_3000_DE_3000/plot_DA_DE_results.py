from numpy import array, arange, argmin, sum, mean, var, size, zeros,	where, histogram
from numpy.random import normal
from matplotlib.pyplot import figure, plot, hist, bar, xlabel, ylabel,title, show, savefig
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize
from past.builtins import xrange
import pandas as pd
import time



def main():
    from numpy import genfromtxt
    data_rg = genfromtxt("../test_codes_hopt/bid-rg-idx-3.xvg"
                            ,skip_header=22,
                            dtype={'names':['time', 'rg','rgx','rgy','rgz'],
                           'formats':[int,float,float,float,float]})
    data_rmsd = genfromtxt("../test_codes_hopt/bid-rmsd-idx-46_3.xvg",skip_header=13,
                            dtype={'names':['time', 'rmsd'],
                           'formats':[int,float]})
    y=data_rmsd['rmsd']
    x=data_rg['rg']


    print("\nCalculando:")

    fig = figure()
    markerX='-o'
    markerY='-x'
    d = pd.read_csv('output_experiment_DA_3000_DE_3000.csv')
    bin_x_DA = d['bin_x_DA']
    bin_y_DA = d['bin_y_DA']
    bin_x_DE = d['bin_x_DE']
    bin_y_DE = d['bin_y_DE']
    plt.plot(bin_x_DA, markerX, color='#1f77b4',linewidth=0.5)
    plt.plot(bin_y_DA, markerY, color='#1f77b4',linewidth=0.5)
    plt.plot(bin_x_DE, markerX, color='#ff7f0e',linewidth=0.5)
    plt.plot(bin_y_DE, markerY, color='#ff7f0e',linewidth=0.5)
    plt.xlabel('Number of executions of algorithms')
    plt.ylabel('Bins in minimum of cost function')
    plt.legend(['Bins for x from DA','Bins for y from DA','Bins for x from DE','Bins for y from DE'])
    plt.suptitle("Dual Annealing (DA) and Differential Evolution (DE) minimization results",fontsize=11)
    plt.title("DA with max interactions of 3000 and DE with max interactions of 3000",fontsize=9)

    fig = figure()
    markerX='-o'
    markerY='-x'
    d = pd.read_csv('output_experiment_DA_3000_DE_3000.csv')
    time_DA = d['time_DA']
    time_DE = d['time_DE']
    plt.plot(time_DA, markerX, color='#2ca02c',linewidth=0.5)
    plt.plot(time_DE, markerY, color='#bcbd22',linewidth=0.5)
    plt.xlabel('Number of executions of algorithms')
    plt.ylabel('Total time (sec) of every executions of algorithms')
    plt.legend(['Time of DA','Time of DE'])
    plt.suptitle("Dual Annealing (DA) and Differential Evolution (DE) execution time",fontsize=11)
    plt.title("DA with max interactions of 3000 and DE with max interactions of 3000",fontsize=9)

    plt.show()



if __name__ == '__main__':
    main()
