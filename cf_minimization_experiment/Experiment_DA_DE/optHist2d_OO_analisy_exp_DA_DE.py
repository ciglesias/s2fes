from numpy import array, arange, argmin, sum, mean, var, size, zeros,	where, histogram
from numpy.random import normal
from matplotlib.pyplot import figure, plot, hist, bar, xlabel, ylabel,title, show, savefig
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize
from past.builtins import xrange
import pandas as pd
import time
from numpy import genfromtxt
import collections
from astropy.visualization import hist

#https://www2.hawaii.edu/~jonghyun/classes/S18/CEE696/files/14_global_optimization.pdf
# https://perso.crans.org/besson/publis/notebooks/Simulated_annealing_in_Python.html
# https://pablormier.github.io/2017/09/05/a-tutorial-on-differential-evolution-with-python/
# https://matplotlib.org/3.2.1/gallery/index.html
# https://plotly.com/python/2D-Histogram/#2d-histogram-overlaid-with-a-scatter-chart
# https://stackoverflow.com/questions/27156381/python-creating-a-2d-histogram-from-a-numpy-matrix


class Opthist2d():


    def __init__(self,data_x,data_y):

        self.data_x=data_x
        self.data_y=data_y
        self.x_max = max(data_x)
        self.x_min = min(data_x)
        self.y_max = max(data_y)
        self.y_min = min(data_y)
        self.Nx_MIN = 1   #Minimum number of bins in x (integer)
        self.Nx_MAX = 100  #Maximum number of bins in x (integer)
        self.Ny_MIN = 1 #Minimum number of bins in y (integer)
        self.Ny_MAX = 100  #Maximum number of bins in y (integer)
        self.Dxy=[]
        self.Cxy=[]
        self.Cxy_Dxy_plot =[] #save data to plot in scatterplot x,y,z


    def sample_resolution(self,data):
        """Compute and return the sample resolution of some data"""
        buf = abs(np.diff(np.sort(data)))
        dx = np.min(buf[buf != 0])
        return dx

    def binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / float(samp_resol)
        return N_max

    def large_binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / samp_resol/ 2
        return int(N_max)

    def arrayofbins(self,N_MIN,N_MAX):
        """Return a array of bins """
        return arange(N_MIN, N_MAX) # #of Bins

    def arrayofbinwidths(self,datamax,datamin, N):
        """Return a array of bins """
        return (datamax - datamin) / N  #Bin size vector

    def matrixofbinwidths(self,Dx,Dy):
        """Set the matrix of binwidths and save in self.Dxy"""
        for i in Dx:    #Bin size vector
            a=[]
            for j in Dy:    #Bin size vector
                a.append((i,j))
            self.Dxy.append(a)
        self.Dxy=array(self.Dxy, dtype=[('x', float),('y', float)]) #matrix of bin size vector
        return self.Dxy

    def cost_function(self,Nx,Ny):
        """ Compute the cost function pairs of binwidths """
        self.Cxy=zeros(np.shape(self.Dxy))
        #Computation of the cost function to x and y
        for i in xrange(size(Nx)):
            for j in xrange(size(Ny)):
                ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx[i],Ny[j]))
                ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
                k = mean(ki) #Mean of event count
                v = var(ki)  #Variance of event count
                self.Cxy[i,j] = (2 * k - v) / ( (self.Dxy[i,j][0]*self.Dxy[i,j][1])**2 )  #The cost Function
                            #(Cxy      , Dx          ,  Dy)
                self.Cxy_Dxy_plot.append((self.Cxy[i,j] , self.Dxy[i,j][0] , self.Dxy[i,j][1]))#Save result of cost function to scatterplot
        self.Cxy_Dxy_plot = array( self.Cxy_Dxy_plot , dtype=[('Cxy', float),('Dx', float), ('Dy', float)])  #Save result of cost function to scatterplot
        print( "ok")

    def cftest(self,N):
        """ Compute the cost function using brute force """
        Nx,Ny=N

        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        #self.v=self.v+1
       #print( self.v
        return Cxy

    def cf(self,N):
        """ Compute the cost function using brute force """
        Nx,Ny=N
        Nx=max(Nx, 1)
        Ny=max(Ny, 1)
        Nx=int(Nx)
        Ny=int(Ny)
        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        # print (k,v, (2 * k - v))
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        #import ipdb; ipdb.set_trace()
        #self.v=self.v+1
        #print( self.v
        # print(N,Nx,Ny,Cxy)
        return Cxy




    def idx_min_cost_function(self):
        """return the index of combination of i and j that produces the minimum cost function"""
        idx_min_Cxy=np.where(self.Cxy == np.min(self.Cxy)) #get the index of the min Cxy
        return (idx_min_Cxy[0][0],idx_min_Cxy[1][0])




def main():



    #------------------------------------------- plot histogram with performance of DA and DE -------------------------------------------

    correct_bin_x=80
    correct_bin_y=45

    result_experiment_DA_10000_DE_10000 = genfromtxt("/Users/cristovao/Devel/project_S2FES/s2fes/cf_minimization/Experiment_DA_DE/experiment_DA_10000_DE_10000/output_experiment_DA_10000_DE_10000.csv",
                            delimiter=",",
                            skip_header=1,
                            dtype={'names':['bin_x_DA','bin_y_DA','time_DA','bin_x_DE','bin_y_DE','time_DE'],
                           'formats':[float,float,float,float,float,float]})

    result_experiment_DA_10000_binx=(result_experiment_DA_10000_DE_10000['bin_x_DA'] > (correct_bin_x-79)) & (result_experiment_DA_10000_DE_10000['bin_x_DA'] < (correct_bin_x+1))
    result_experiment_DA_10000_biny=(result_experiment_DA_10000_DE_10000['bin_y_DA'] > (correct_bin_y-1)) & (result_experiment_DA_10000_DE_10000['bin_y_DA'] < (correct_bin_y+1))
    result_experiment_DA_10000_binx_biny=zip(result_experiment_DA_10000_binx,result_experiment_DA_10000_biny)
    result_experiment_DA_10000_binx_biny=collections.Counter(result_experiment_DA_10000_binx_biny)

    result_experiment_DE_10000_binx=(result_experiment_DA_10000_DE_10000['bin_x_DE'] > (correct_bin_x-79)) & (result_experiment_DA_10000_DE_10000['bin_x_DE'] < (correct_bin_x+1))
    result_experiment_DE_10000_biny=(result_experiment_DA_10000_DE_10000['bin_y_DE'] > (correct_bin_y-1)) & (result_experiment_DA_10000_DE_10000['bin_y_DE'] < (correct_bin_y+1))
    result_experiment_DE_10000_binx_biny=zip(result_experiment_DE_10000_binx,result_experiment_DE_10000_biny)
    result_experiment_DE_10000_binx_biny=collections.Counter(result_experiment_DE_10000_binx_biny)

    result_experiment_DA_10000_binx_biny=result_experiment_DA_10000_binx_biny[(True,True)]
    result_experiment_DE_10000_binx_biny=result_experiment_DE_10000_binx_biny[(True,True)]

    result_experiment_DA_8000_DE_5000 = genfromtxt("/Users/cristovao/Devel/project_S2FES/s2fes/cf_minimization/Experiment_DA_DE/experiment_DA_8000_DE_5000/output_experiment_DA_8000_DE_5000.csv",
                            delimiter=",",
                            skip_header=1,
                            dtype={'names':['bin_x_DA','bin_y_DA','time_DA','bin_x_DE','bin_y_DE','time_DE'],
                           'formats':[float,float,float,float,float,float]})

    result_experiment_DA_8000_binx=(result_experiment_DA_8000_DE_5000['bin_x_DA'] > (correct_bin_x-79)) & (result_experiment_DA_8000_DE_5000['bin_x_DA'] < (correct_bin_x+1))
    result_experiment_DA_8000_biny=(result_experiment_DA_8000_DE_5000['bin_y_DA'] > (correct_bin_y-1)) & (result_experiment_DA_8000_DE_5000['bin_y_DA'] < (correct_bin_y+1))
    result_experiment_DA_8000_binx_biny=zip(result_experiment_DA_8000_binx,result_experiment_DA_8000_biny)
    result_experiment_DA_8000_binx_biny=collections.Counter(result_experiment_DA_8000_binx_biny)

    result_experiment_DE_5000_binx=(result_experiment_DA_8000_DE_5000['bin_x_DE'] > (correct_bin_x-79)) & (result_experiment_DA_8000_DE_5000['bin_x_DE'] < (correct_bin_x+1))
    result_experiment_DE_5000_biny=(result_experiment_DA_8000_DE_5000['bin_y_DE'] > (correct_bin_y-1)) & (result_experiment_DA_8000_DE_5000['bin_y_DE'] < (correct_bin_y+1))
    result_experiment_DE_5000_binx_biny=zip(result_experiment_DE_5000_binx,result_experiment_DE_5000_biny)
    result_experiment_DE_5000_binx_biny=collections.Counter(result_experiment_DE_5000_binx_biny)

    result_experiment_DA_8000_binx_biny=result_experiment_DA_8000_binx_biny[(True,True)]
    result_experiment_DE_5000_binx_biny=result_experiment_DE_5000_binx_biny[(True,True)]

    result_experiment_DA_8000_DE_4000 = genfromtxt("/Users/cristovao/Devel/project_S2FES/s2fes/cf_minimization/Experiment_DA_DE/experiment_DA_8000_DE_4000/output_experiment_DA_8000_DE_4000.csv",
                            delimiter=",",
                            skip_header=1,
                            dtype={'names':['bin_x_DA','bin_y_DA','time_DA','bin_x_DE','bin_y_DE','time_DE'],
                           'formats':[float,float,float,float,float,float]})

    result_experiment_DA_8000_binx=(result_experiment_DA_8000_DE_4000['bin_x_DA'] > (correct_bin_x-79)) & (result_experiment_DA_8000_DE_4000['bin_x_DA'] < (correct_bin_x+1))
    result_experiment_DA_8000_biny=(result_experiment_DA_8000_DE_4000['bin_y_DA'] > (correct_bin_y-1)) & (result_experiment_DA_8000_DE_4000['bin_y_DA'] < (correct_bin_y+1))
    result_experiment1_DA_8000_binx_biny=zip(result_experiment_DA_8000_binx,result_experiment_DA_8000_biny)
    result_experiment1_DA_8000_binx_biny=collections.Counter(result_experiment1_DA_8000_binx_biny)

    result_experiment_DE_4000_binx=(result_experiment_DA_8000_DE_4000['bin_x_DE'] > (correct_bin_x-79)) & (result_experiment_DA_8000_DE_4000['bin_x_DE'] < (correct_bin_x+1))
    result_experiment_DE_4000_biny=(result_experiment_DA_8000_DE_4000['bin_y_DE'] > (correct_bin_y-1)) & (result_experiment_DA_8000_DE_4000['bin_y_DE'] < (correct_bin_y+1))
    result_experiment_DE_4000_binx_biny=zip(result_experiment_DE_4000_binx,result_experiment_DE_4000_biny)
    result_experiment_DE_4000_binx_biny=collections.Counter(result_experiment_DE_4000_binx_biny)

    result_experiment1_DA_8000_binx_biny=result_experiment1_DA_8000_binx_biny[(True,True)]
    result_experiment_DE_4000_binx_biny=result_experiment_DE_4000_binx_biny[(True,True)]

    result_experiment_DA_3000_DE_3000 = genfromtxt("/Users/cristovao/Devel/project_S2FES/s2fes/cf_minimization/Experiment_DA_DE/experiment_DA_3000_DE_3000/output_experiment_DA_3000_DE_3000.csv",
                            delimiter=",",
                            skip_header=1,
                            dtype={'names':['bin_x_DA','bin_y_DA','time_DA','bin_x_DE','bin_y_DE','time_DE'],
                           'formats':[float,float,float,float,float,float]})

    result_experiment_DA_3000_binx=(result_experiment_DA_3000_DE_3000['bin_x_DA'] > (correct_bin_x-79)) & (result_experiment_DA_3000_DE_3000['bin_x_DA'] < (correct_bin_x+1))
    result_experiment_DA_3000_biny=(result_experiment_DA_3000_DE_3000['bin_y_DA'] > (correct_bin_y-1)) & (result_experiment_DA_3000_DE_3000['bin_y_DA'] < (correct_bin_y+1))
    result_experiment_DA_3000_binx_biny=zip(result_experiment_DA_3000_binx,result_experiment_DA_3000_biny)
    result_experiment_DA_3000_binx_biny=collections.Counter(result_experiment_DA_3000_binx_biny)

    result_experiment_DE_3000_binx=(result_experiment_DA_3000_DE_3000['bin_x_DE'] > (correct_bin_x-79)) & (result_experiment_DA_3000_DE_3000['bin_x_DE'] < (correct_bin_x+1))
    result_experiment_DE_3000_biny=(result_experiment_DA_3000_DE_3000['bin_y_DE'] > (correct_bin_y-1)) & (result_experiment_DA_3000_DE_3000['bin_y_DE'] < (correct_bin_y+1))
    result_experiment_DE_3000_binx_biny=zip(result_experiment_DE_3000_binx,result_experiment_DE_3000_biny)
    result_experiment_DE_3000_binx_biny=collections.Counter(result_experiment_DE_3000_binx_biny)

    result_experiment_DA_3000_binx_biny=result_experiment_DA_3000_binx_biny[(True,True)]
    result_experiment_DE_3000_binx_biny=result_experiment_DE_3000_binx_biny[(True,True)]

    labels = ['RUN1 - DA with 3000 interactions', 'RUN1 - DE with 3000 interactions',
            'RUN2 - DA with 8000 interactions', 'RUN2 - DE with 4000 interactions',
            'RUN3 - DA with 8000 interactions', 'RUN3 - DE with 5000 interactions',
            'RUN4 - DA with 10000 interactions', 'RUN4 - DE with 10000 interactions']
    failures = [1000-result_experiment_DA_3000_binx_biny,
                    1000-result_experiment_DE_3000_binx_biny,
                    1000-result_experiment1_DA_8000_binx_biny,
                    1000-result_experiment_DE_4000_binx_biny,
                    1000-result_experiment_DA_8000_binx_biny,
                    1000-result_experiment_DE_5000_binx_biny,
                    1000-result_experiment_DA_10000_binx_biny,
                    1000-result_experiment_DE_10000_binx_biny]
    successes = [result_experiment_DA_3000_binx_biny,
                    result_experiment_DE_3000_binx_biny,
                    result_experiment1_DA_8000_binx_biny,
                    result_experiment_DE_4000_binx_biny,
                    result_experiment_DA_8000_binx_biny,
                    result_experiment_DE_5000_binx_biny,
                    result_experiment_DA_10000_binx_biny,
                    result_experiment_DE_10000_binx_biny]


    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.barh(x - width/2, failures, width, label='failures')
    rects2 = ax.barh(x + width/2, successes, width, label='successes')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_xlabel('Number independent executions of algorithms')
    ax.set_title('Dual Annealing (DA) and Differential Evolution (DE) minimization results')
    ax.set_yticks(x)
    ax.set_yticklabels(labels)
    ax.legend()

    for i, v in enumerate(failures):
        ax.text(v + 3, i - .25, str(v), color='black')
    for i, v in enumerate(successes):
        ax.text(v + 3, i + .15, str(v), color='black')

    fig.tight_layout()
    plt.show()



    #------------------------------------------- plot histogram total time -------------------------------------------

    fig, axs = plt.subplots(4, 2, sharey=True, tight_layout=True)
    print(axs)
    counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DA'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[0][0].hist(result_experiment_DA_10000_DE_10000['time_DA'], bins=len(counts),color='#00cc66', rwidth=0.9)
    axs[0][0].set_xlabel('time(sec)')
    axs[0][0].set_ylabel('Number of executions of algorithms')
    axs[0][0].set_title('Total time (sec) of every executions of DA with 10000 (RUN4)')
    axs[0][0].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_10000_DE_10000['time_DA']),max(result_experiment_DA_10000_DE_10000['time_DA']),min(result_experiment_DA_10000_DE_10000['time_DA'])), transform=axs[0][0].transAxes,ha='right', va='top', fontsize=11)
    counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[0][1].hist(result_experiment_DA_10000_DE_10000['time_DE'], bins=len(counts),color='#0066cc', rwidth=0.9)
    axs[0][1].set_xlabel('time(sec)')
    axs[0][1].set_title('Total time (sec) of every executions of DE with 10000 (RUN4)')
    axs[0][1].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_10000_DE_10000['time_DE']),max(result_experiment_DA_10000_DE_10000['time_DE']),min(result_experiment_DA_10000_DE_10000['time_DE'])), transform=axs[0][1].transAxes,ha='right', va='top', fontsize=11)

    counts, bins, patches = hist(result_experiment_DA_8000_DE_5000['time_DA'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[1][0].hist(result_experiment_DA_8000_DE_5000['time_DA'], bins=len(counts),color='#00cc66', rwidth=0.9)
    axs[1][0].set_xlabel('time(sec)')
    axs[1][0].set_ylabel('Number of executions of algorithms')
    axs[1][0].set_title('Total time (sec) of every executions of DA with 8000 (RUN3)')
    axs[1][0].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_8000_DE_5000['time_DA']),max(result_experiment_DA_8000_DE_5000['time_DA']),min(result_experiment_DA_8000_DE_5000['time_DA'])), transform=axs[1][0].transAxes,ha='right', va='top', fontsize=11)
    counts, bins, patches = hist(result_experiment_DA_8000_DE_5000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[1][1].hist(result_experiment_DA_8000_DE_5000['time_DE'], bins=len(counts),color='#0066cc', rwidth=0.9)
    axs[1][1].set_xlabel('time(sec)')
    axs[1][1].set_title('Total time (sec) of every executions of DE with 5000 (RUN3)')
    axs[1][1].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_8000_DE_5000['time_DE']),max(result_experiment_DA_8000_DE_5000['time_DE']),min(result_experiment_DA_8000_DE_5000['time_DE'])), transform=axs[1][1].transAxes,ha='right', va='top', fontsize=11)

    counts, bins, patches = hist(result_experiment_DA_8000_DE_4000['time_DA'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[2][0].hist(result_experiment_DA_8000_DE_4000['time_DA'], bins=len(counts),color='#00cc66', rwidth=0.9)
    axs[2][0].set_xlabel('time(sec)')
    axs[2][0].set_ylabel('Number of executions of algorithms')
    axs[2][0].set_title('Total time (sec) of every executions of DA with 8000 (RUN2)')
    axs[2][0].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_8000_DE_4000['time_DA']),max(result_experiment_DA_8000_DE_4000['time_DA']),min(result_experiment_DA_8000_DE_4000['time_DA'])), transform=axs[2][0].transAxes,ha='right', va='top', fontsize=11)
    counts, bins, patches = hist(result_experiment_DA_8000_DE_4000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[2][1].hist(result_experiment_DA_8000_DE_4000['time_DE'], bins=len(counts),color='#0066cc', rwidth=0.9)
    axs[2][1].set_xlabel('time(sec)')
    axs[2][1].set_title('Total time (sec) of every executions of DE with 4000 (RUN2)')
    axs[2][1].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_8000_DE_4000['time_DE']),max(result_experiment_DA_8000_DE_4000['time_DE']),min(result_experiment_DA_8000_DE_4000['time_DE'])), transform=axs[2][1].transAxes,ha='right', va='top', fontsize=11)

    counts, bins, patches = hist(result_experiment_DA_3000_DE_3000['time_DA'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[3][0].hist(result_experiment_DA_3000_DE_3000['time_DA'], bins=len(counts),color='#00cc66', rwidth=0.9)
    axs[3][0].set_xlabel('time(sec)')
    axs[3][0].set_ylabel('Number of executions of algorithms')
    axs[3][0].set_title('Total time (sec) of every executions of DA with 3000 (RUN1)')
    axs[3][0].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_3000_DE_3000['time_DA']),max(result_experiment_DA_3000_DE_3000['time_DA']),min(result_experiment_DA_3000_DE_3000['time_DA'])), transform=axs[3][0].transAxes,ha='right', va='top', fontsize=11)
    counts, bins, patches = hist(result_experiment_DA_3000_DE_3000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    axs[3][1].hist(result_experiment_DA_3000_DE_3000['time_DE'], bins=len(counts),color='#0066cc', rwidth=0.9)
    axs[3][1].set_xlabel('time(sec)')
    axs[3][1].set_title('Total time (sec) of every executions of DE with 3000 (RUN1)')
    axs[3][1].text(0.99, 0.95, 'Number of bins: %i\nMean: %f sec\nTotal time max: %f sec\nTotal time min: %f sec' % (len(counts),mean(result_experiment_DA_3000_DE_3000['time_DE']),max(result_experiment_DA_3000_DE_3000['time_DE']),min(result_experiment_DA_3000_DE_3000['time_DE'])),transform=axs[3][1].transAxes,ha='right', va='top', fontsize=11)







    # fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)
    # counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    # axs[0].hist(result_experiment_DA_8000_DE_5000['time_DA'], bins=len(counts))
    # counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    # axs[1].hist(result_experiment_DA_8000_DE_5000['time_DE'], bins=len(counts))
    #
    # fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)
    # counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    # axs[0].hist(result_experiment_DA_8000_DE_4000['time_DA'], bins=len(counts))
    # counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    # axs[1].hist(result_experiment_DA_8000_DE_4000['time_DE'], bins=len(counts))
    #
    # fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)
    # counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    # axs[0].hist(result_experiment_DA_3000_DE_3000['time_DA'], bins=len(counts))
    # counts, bins, patches = hist(result_experiment_DA_10000_DE_10000['time_DE'], bins='freedman', ax=None, histtype='stepfilled', alpha=0.2, density=True)
    # axs[1].hist(result_experiment_DA_3000_DE_3000['time_DE'], bins=len(counts))

    #fig.tight_layout()
    plt.show()








    counts, bins, patches = hist(data, bins=style,ax=ax, color='k', histtype='step', normed=False , range=(float(mmin), float(mmax)))  # IMP


    # generate some complicated data
    rng = np.random.RandomState(0)
    t = np.concatenate([-5 + 1.8 * rng.standard_cauchy(500),
                        -4 + 0.8 * rng.standard_cauchy(2000),
                        -1 + 0.3 * rng.standard_cauchy(500),
                        2 + 0.8 * rng.standard_cauchy(1000),
                        4 + 1.5 * rng.standard_cauchy(1000)])

    # truncate to a reasonable range
    t = t[(t > -15) & (t < 15)]

    # draw histograms with two different bin widths
    fig, ax = plt.subplots(1, 2, figsize=(10, 4))

    fig.subplots_adjust(left=0.1, right=0.95, bottom=0.15)
    for i, bins in enumerate(['scott', 'freedman']):
        counts, bins, patches = hist(result_experiment_DA_3000_DE_3000['time_DE'], bins=bins, ax=ax[i], histtype='stepfilled', alpha=0.2, density=True)
        ax[i].set_xlabel('t')
        ax[i].set_ylabel('P(t)')
        ax[i].set_title('hist(t, bins="{0}")'.format(bins), fontdict=dict(family='monospace'))

    fig.tight_layout()
    plt.show()
    import ipdb; ipdb.set_trace()

    # data_rg = genfromtxt("test_codes_hopt/bid-rg-idx-3.xvg"
    #                         ,skip_header=22,
    #                         dtype={'names':['time', 'rg','rgx','rgy','rgz'],
    #                        'formats':[int,float,float,float,float]})
    # data_rmsd = genfromtxt("test_codes_hopt/bid-rmsd-idx-46_3.xvg",skip_header=13,
    #                         dtype={'names':['time', 'rmsd'],
    #                        'formats':[int,float]})
    y=data_rmsd['rmsd']
    x=data_rg['rg']

    H=Opthist2d(x,y)

    dx=H.sample_resolution(H.data_x)
    dy=H.sample_resolution(H.data_y)
    print("sample_resolution of X and Y: ",dx,dy)

    Nx_max=H.large_binwidth(H.x_max, H.x_min,dx)
    Ny_max=H.large_binwidth(H.y_max, H.y_min,dy)
    print("large_binwidth of X and Y: ",Nx_max,Ny_max)

    Nx_max=1000
    Ny_max=1000

    start_point=[100.,100.]
    bounds = [(1, Nx_max), (1, Ny_max)]

    #ranges=(slice(1, 100,1),) * 2
    # print(1)
    # ranges=(slice(1, Nx_max,1),slice(1, Ny_max,1))
    # print(2)

    # # output=optimize.brute(H.cf, ((1, 100,1), (1, 100,1)),full_output=True)
    # print("brute")
    # output=optimize.brute(H.cf,ranges, disp=True, finish=None)
    # print(output)



    print("\nCalculando:")


    file = open('output1.csv','w')
    file.write("bin_x_DA,bin_y_DA,time_DA,bin_x_DE,bin_y_DE,time_DE\n")
    maxi=10000
    for i in range(250):
        start_time = time.time()
        res_dual_annealing = optimize.dual_annealing(H.cf, bounds,maxiter=10000,no_local_search="True")
        time_DA="%s" % (time.time() - start_time)
        print("_____dual_annealing_____\n",res_dual_annealing)
        #print("\n_____dual_annealing_____")
        start_time = time.time()
        res_differential_evolution = optimize.differential_evolution(H.cf, bounds, strategy='best1bin',
                maxiter=maxi,popsize=4000, tol=0.01, mutation=(0,1),recombination=0.7,
                seed=None,callback=None, disp=False, polish=True,init='latinhypercube', atol=0)
        time_DE="%s" % (time.time() - start_time)
        print("_____differential_evolution_____\n",res_differential_evolution)
        #print("\n_____differential_evolution____")
        print(i)
        file.write(str(res_dual_annealing['x'][0])+","+ str(res_dual_annealing['x'][1])+","+time_DA+","+str(res_differential_evolution['x'][0])+","+ str(res_differential_evolution['x'][1])+","+time_DE+"\n")
    file.close()


    # arquivo = open('output1.csv','w')
    # arquivo.write("bin_x_DA,bin_y_DA,bin_x_DE,bin_y_DE\n")
    # arquivo.write("1,1,3,5\n")
    # arquivo.write("2,4,4,6\n")
    # arquivo.write("3,6,6,8\n")
    # arquivo.write("4,8,7,5\n")
    # arquivo.write("5,9,8,7\n")
    # arquivo.close()

    fig = figure()
    markerX='-ok'
    markerY='-xk'
    d = pd.read_csv('output1.csv')
    bin_x_DA = d['bin_x_DA']
    bin_y_DA = d['bin_y_DA']
    bin_x_DE = d['bin_x_DE']
    bin_y_DE = d['bin_y_DE']
    plt.plot(bin_x_DA, markerX, color='r')
    plt.plot(bin_y_DA, markerY, color='r')
    plt.plot(bin_x_DE, markerX, color='g')
    plt.plot(bin_y_DE, markerY, color='g')
    plt.xlabel('#')
    plt.ylabel('Number of Bins(int)')
    plt.legend(['Bins x from DA','Bins y from DA','Bins x from DE','Bins y from DE'])

    fig = figure()
    markerX='-ok'
    markerY='-xk'
    d = pd.read_csv('output1.csv')
    time_DA = d['time_DA']
    time_DE = d['time_DE']
    plt.plot(time_DA, markerX, color='blue')
    plt.plot(time_DE, markerY, color='black')
    plt.xlabel('#')
    plt.ylabel('time(s))')
    plt.legend(['Time from DA','Time from DE'])


    plt.show()


    result_experiment_DA_10000_DE_10000['bin_x_DA']

    N_points = 100000
    n_bins = 20

    # Generate a normal distribution, center at x=0 and y=5
    x = np.random.randn(N_points)
    y = .4 * x + np.random.randn(100000) + 5

    fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)

    # We can set the number of bins with the `bins` kwarg
    axs[0].hist(result_experiment_DA_10000_DE_10000['bin_x_DA'], bins=n_bins)
    axs[1].hist(result_experiment_DA_10000_DE_10000['bin_x_DE'], bins=n_bins)




    # # Fixing random state for reproducibility
    # np.random.seed(19680801)
    #
    #
    # x = np.random.rand(10)
    # y = np.random.rand(10)
    # z = np.sqrt(x**2 + y**2)

    plt.subplot(21)
    plt.hist(x, y, s=80, c=z, marker=">")

    plt.subplot(22)
    plt.hist(x, y, s=80, c=z, marker=(5, 0))

    plt.subplot(23)
    plt.hist(x, y, s=80, c=z, marker=verts)

    plt.subplot(24)
    plt.hist(x, y, s=80, c=z, marker=(5, 1))

    plt.subplot(25)
    plt.hist(x, y, s=80, c=z, marker='+')

    plt.subplot(26)
    plt.hist(x, y, s=80, c=z, marker=(5, 2))

    plt.show()




if __name__ == '__main__':
    main()
