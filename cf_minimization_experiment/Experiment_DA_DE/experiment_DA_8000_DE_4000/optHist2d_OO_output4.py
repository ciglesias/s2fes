from numpy import array, arange, argmin, sum, mean, var, size, zeros,	where, histogram
from numpy.random import normal
from matplotlib.pyplot import figure, plot, hist, bar, xlabel, ylabel,title, show, savefig
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize
from past.builtins import xrange
import pandas as pd
import time

class Opthist2d():


    def __init__(self,data_x,data_y):

        self.data_x=data_x
        self.data_y=data_y
        self.x_max = max(data_x)
        self.x_min = min(data_x)
        self.y_max = max(data_y)
        self.y_min = min(data_y)
        self.Nx_MIN = 1   #Minimum number of bins in x (integer)
        self.Nx_MAX = 100  #Maximum number of bins in x (integer)
        self.Ny_MIN = 1 #Minimum number of bins in y (integer)
        self.Ny_MAX = 100  #Maximum number of bins in y (integer)
        self.Dxy=[]
        self.Cxy=[]
        self.Cxy_Dxy_plot =[] #save data to plot in scatterplot x,y,z


    def sample_resolution(self,data):
        """Compute and return the sample resolution of some data"""
        buf = abs(np.diff(np.sort(data)))
        dx = np.min(buf[buf != 0])
        return dx

    def binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / float(samp_resol)
        return N_max

    def large_binwidth(self,datamax,datamin,samp_resol):
        """The user should search the binwidth that is larger than the sampling resolution of your data"""
        N_max =  (datamax-datamin) / samp_resol/ 2
        return int(N_max)

    def arrayofbins(self,N_MIN,N_MAX):
        """Return a array of bins """
        return arange(N_MIN, N_MAX) # #of Bins

    def arrayofbinwidths(self,datamax,datamin, N):
        """Return a array of bins """
        return (datamax - datamin) / N  #Bin size vector

    def matrixofbinwidths(self,Dx,Dy):
        """Set the matrix of binwidths and save in self.Dxy"""
        for i in Dx:    #Bin size vector
            a=[]
            for j in Dy:    #Bin size vector
                a.append((i,j))
            self.Dxy.append(a)
        self.Dxy=array(self.Dxy, dtype=[('x', float),('y', float)]) #matrix of bin size vector
        return self.Dxy

    def cost_function(self,Nx,Ny):
        """ Compute the cost function pairs of binwidths """
        self.Cxy=zeros(np.shape(self.Dxy))
        #Computation of the cost function to x and y
        for i in xrange(size(Nx)):
            for j in xrange(size(Ny)):
                ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx[i],Ny[j]))
                ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
                k = mean(ki) #Mean of event count
                v = var(ki)  #Variance of event count
                self.Cxy[i,j] = (2 * k - v) / ( (self.Dxy[i,j][0]*self.Dxy[i,j][1])**2 )  #The cost Function
                            #(Cxy      , Dx          ,  Dy)
                self.Cxy_Dxy_plot.append((self.Cxy[i,j] , self.Dxy[i,j][0] , self.Dxy[i,j][1]))#Save result of cost function to scatterplot
        self.Cxy_Dxy_plot = array( self.Cxy_Dxy_plot , dtype=[('Cxy', float),('Dx', float), ('Dy', float)])  #Save result of cost function to scatterplot
        print( "ok")

    def cftest(self,N):
        """ Compute the cost function using brute force """
        Nx,Ny=N

        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        #self.v=self.v+1
       #print( self.v
        return Cxy

    def cf(self,N):
        """ Compute the cost function using brute force """
        Nx,Ny=N
        Nx=max(Nx, 1)
        Ny=max(Ny, 1)
        Nx=int(Nx)
        Ny=int(Ny)
        Dx=(self.x_max - self.x_min)/float(Nx)
        Dy=(self.y_max - self.y_min)/float(Ny)
        #Computation of the cost function to x and y
        ki = np.histogram2d(self.data_x,self.data_y, bins=(Nx,Ny))
        ki = ki[0]   #The mean and the variance are simply computed from the event counts in all the bins of the 2-dimensional histogram.
        k = mean(ki) #Mean of event count
        v = var(ki)  #Variance of event count
        # print (k,v, (2 * k - v))
        Cxy = (2 * k - v) / ( (Dx*Dy)**2 )  #The cost Function
        #import ipdb; ipdb.set_trace()
        #self.v=self.v+1
        #print( self.v
        # print(N,Nx,Ny,Cxy)
        return Cxy




    def idx_min_cost_function(self):
        """return the index of combination of i and j that produces the minimum cost function"""
        idx_min_Cxy=np.where(self.Cxy == np.min(self.Cxy)) #get the index of the min Cxy
        return (idx_min_Cxy[0][0],idx_min_Cxy[1][0])




def main():
    #COMPUTE THE COST FUNCTION
    #-------------------------
    from numpy import genfromtxt
    data_rg = genfromtxt("../test_codes_hopt/bid-rg-idx-3.xvg"
                            ,skip_header=22,
                            dtype={'names':['time', 'rg','rgx','rgy','rgz'],
                           'formats':[int,float,float,float,float]})
    data_rmsd = genfromtxt("../test_codes_hopt/bid-rmsd-idx-46_3.xvg",skip_header=13,
                            dtype={'names':['time', 'rmsd'],
                           'formats':[int,float]})
    y=data_rmsd['rmsd']
    x=data_rg['rg']

    H=Opthist2d(x,y)

    dx=H.sample_resolution(H.data_x)
    dy=H.sample_resolution(H.data_y)
    print("sample_resolution of X and Y: ",dx,dy)

    Nx_max=H.large_binwidth(H.x_max, H.x_min,dx)
    Ny_max=H.large_binwidth(H.y_max, H.y_min,dy)
    print("large_binwidth of X and Y: ",Nx_max,Ny_max)

    Nx_max=1000
    Ny_max=1000

    start_point=[100.,100.]
    bounds = [(1, Nx_max), (1, Ny_max)]

    #ranges=(slice(1, 100,1),) * 2
    # print(1)
    # ranges=(slice(1, Nx_max,1),slice(1, Ny_max,1))
    # print(2)

    # # output=optimize.brute(H.cf, ((1, 100,1), (1, 100,1)),full_output=True)
    # print("brute")
    # output=optimize.brute(H.cf,ranges, disp=True, finish=None)
    # print(output)



    print("\nCalculando:")


    file = open('output4.csv','w')
    file.write("bin_x_DA,bin_y_DA,time_DA,bin_x_DE,bin_y_DE,time_DE\n")
    maxi=4000
    for i in range(250):
        start_time = time.time()
        res_dual_annealing = optimize.dual_annealing(H.cf, bounds,maxiter=8000,no_local_search="True")
        time_DA="%s" % (time.time() - start_time)
        #print("\n_____dual_annealing_____",res_dual_annealing)
        print("\n_____dual_annealing_____")
        start_time = time.time()
        res_differential_evolution = optimize.differential_evolution(H.cf, bounds, strategy='best1bin',
                maxiter=maxi,popsize=2000, tol=0.01, mutation=(0,1),recombination=0.7,
                seed=None,callback=None, disp=False, polish=True,init='latinhypercube', atol=0)
        time_DE="%s" % (time.time() - start_time)
        #print("\n_____differential_evolution_____",res_differential_evolution)
        print("\n_____differential_evolution____")
        print(i)
        file.write(str(res_dual_annealing['x'][0])+","+ str(res_dual_annealing['x'][1])+","+time_DA+","+str(res_differential_evolution['x'][0])+","+ str(res_differential_evolution['x'][1])+","+time_DE+"\n")
    file.close()


    #
    # arquivo = open('output1.csv','w')
    # arquivo.write("bin_x_DA,bin_y_DA,bin_x_DE,bin_y_DE\n")
    # arquivo.write("1,1,3,5\n")
    # arquivo.write("2,4,4,6\n")
    # arquivo.write("3,6,6,8\n")
    # arquivo.write("4,8,7,5\n")
    # arquivo.write("5,9,8,7\n")
    # arquivo.close()

    # fig = figure()
    # markerX='-ok'
    # markerY='-xk'
    # d = pd.read_csv('output4.csv')
    # bin_x_DA = d['bin_x_DA']
    # bin_y_DA = d['bin_y_DA']
    # bin_x_DE = d['bin_x_DE']
    # bin_y_DE = d['bin_y_DE']
    # plt.plot(bin_x_DA, markerX, color='r')
    # plt.plot(bin_y_DA, markerY, color='r')
    # plt.plot(bin_x_DE, markerX, color='g')
    # plt.plot(bin_y_DE, markerY, color='g')
    # plt.xlabel('#')
    # plt.ylabel('Number of Bins(int)')
    # plt.legend(['Bins x from DA','Bins y from DA','Bins x from DE','Bins y from DE'])
    #
    # fig = figure()
    # markerX='-ok'
    # markerY='-xk'
    # d = pd.read_csv('output4.csv')
    # time_DA = d['time_DA']
    # time_DE = d['time_DE']
    # plt.plot(time_DA, markerX, color='blue')
    # plt.plot(time_DE, markerY, color='black')
    # plt.xlabel('#')
    # plt.ylabel('time(s))')
    # plt.legend(['Time from DA','Time from DE'])
    #
    #
    # plt.show()
    #
    #

    #plot histogram2d
    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[13653,3757779])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')


    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[10000,10000])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')


    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[1000,1000])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')
    #
    #
    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[100,100])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')
    #
    #
    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[80,45])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')
    #
    #
    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[55,51])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')
    #
    #
    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[25,25])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')
    #
    #
    # fig = figure()
    # Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[1,1])
    # Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    # plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    # plt.ylabel("y")
    # plt.xlabel("x")
    # plt.colorbar().set_label('z')
    #
    #
    #
    # plt.show()





    t="""
    #PLOTS
    #-----

    #plot histogram2d
    fig = figure()
    Histog2d, xedges, yedges = np.histogram2d(x, y,bins=[Nx[idx_Nx],Ny[idx_Ny]])
    Hmasked = np.ma.masked_where(Histog2d==0,Histog2d)
    plt.imshow( Hmasked.T,extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ] ,interpolation='nearest',origin='lower',aspect='auto',cmap=plt.cm.Spectral)
    plt.ylabel("y")
    plt.xlabel("x")
    plt.colorbar().set_label('z')
    plt.show()

    #plot scatterplot3d to Dx,Dy and Cxy
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x=H.Cxy_Dxy_plot['Dx']
    y=H.Cxy_Dxy_plot['Dy']
    z =H.Cxy_Dxy_plot['Cxy']
    ax.scatter(x, y, z, c=z, marker='o')
    ax.set_xlabel('Dx')
    ax.set_ylabel('Dy')
    ax.set_zlabel('Cxy')
    plt.draw()
    ax.scatter( [optDx], [optDy],[Cxymin], marker='v', s=150,c="red")
    ax.text(optDx, optDy,Cxymin, "Cxy min", color='red')
    plt.draw()
    plt.show()

  """

if __name__ == '__main__':
    main()
